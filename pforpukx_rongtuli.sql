-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 09, 2017 at 02:15 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pforpukx_rongtuli`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `about_title` longtext COLLATE utf8mb4_unicode_ci,
  `about_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `about_title`, `about_image`, `about_description`, `created_at`, `updated_at`) VALUES
(8, 'Title', NULL, '<p>Birthday Party / Wedding Program</p>', NULL, NULL),
(9, 'Demo', NULL, '<p>add</p>', NULL, NULL),
(10, 'Neww', NULL, '<p>description</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(4, 'WEDDING PLANNING & MANAGEMENT', '1502010838.jpg', '<p>We Offer You An Array Of Services For Wedding Event Management And Co-Ordination While You Can Simply Sit Back And Enjoy The Celebrations And Our Expert Team Takes Care Of All The Arrangements. We Have An Expansive Database And Personal Relationships With Many Of Bangladeshi&#39;s Best Known Vendors And Key Suppliers. So It Easy For Us To Manage The Best Value Of Your Money.</p>', NULL, NULL, '1', NULL, NULL),
(5, 'PHOTO & VIDEOGRAPHY', '1502010970.jpg', '<p>The Blissful Day Of Your Wedding Is A Utopian Time When Two Lives Unite, And Two Hearts Beat As One. And Wedding Photo &amp; Videography Is The Memorializing Of Two People Reveling In Love And Making A Promise To Share A Life &ndash; For A Lifetime! Our Wedding Photography Services Will Preserve Each Magic Moment For All Time!</p>', NULL, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `birthday_categories`
--

CREATE TABLE `birthday_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_type` int(11) DEFAULT NULL,
  `cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `birthday_subcategories`
--

CREATE TABLE `birthday_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_title` longtext COLLATE utf8mb4_unicode_ci,
  `blog_image` longtext COLLATE utf8mb4_unicode_ci,
  `blog_description` longtext COLLATE utf8mb4_unicode_ci,
  `blog_sub_cat_id` int(11) DEFAULT NULL,
  `blog_type_id` int(11) DEFAULT NULL,
  `blog_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `blog_title`, `blog_image`, `blog_description`, `blog_sub_cat_id`, `blog_type_id`, `blog_status`, `created_at`, `updated_at`) VALUES
(17, 'Traditional', '1505799715.jpg', '<p>Traditional</p>', NULL, 0, '1', '2017-09-19 05:41:56', '0000-00-00 00:00:00'),
(18, 'thtun', '1505799704.jpg', '<p>Description</p>', NULL, 0, '1', '2017-09-19 05:41:44', '0000-00-00 00:00:00'),
(20, 'fff', '1505653039.jpg', '<p>fff</p>', NULL, 0, '1', '2017-09-17 12:57:19', NULL),
(21, 'Special offer 1', '1505799686.jpg', '<p>Special offer Description</p>', NULL, 0, '1', '2017-11-30 11:12:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_type` int(11) DEFAULT NULL,
  `cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `cat_type`, `cat_status`, `created_at`, `updated_at`) VALUES
(1, 'name2', 2, '1', NULL, NULL),
(2, 'new', 1, '1', NULL, NULL),
(3, 'demo', 2, '1', NULL, NULL),
(4, 'Cat-2', 1, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `contact_title` longtext COLLATE utf8_unicode_ci,
  `contact_email` longtext COLLATE utf8_unicode_ci,
  `contact_phone` longtext COLLATE utf8_unicode_ci,
  `contact_description` longtext COLLATE utf8_unicode_ci,
  `contact_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `contact_title`, `contact_email`, `contact_phone`, `contact_description`, `contact_image`, `created_at`, `updated_at`) VALUES
(9, 'bd', 'wdw@efef.com', 'db', 'fhb', NULL, '2017-09-26 14:06:33', '2017-09-26 14:06:33');

-- --------------------------------------------------------

--
-- Table structure for table `eventcategories`
--

CREATE TABLE `eventcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_cat_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_description` longtext COLLATE utf8mb4_unicode_ci,
  `event_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `event_image`, `event_description`, `event_date`, `created_at`, `updated_at`) VALUES
(1, 'Mega Event', NULL, '<p>Description</p>', '2017-08-08', NULL, NULL),
(2, 'njnuj', NULL, '<p>ju7j 8kiu8ooki desewr jyujyj weswede&nbsp;ju7j 8kiu8ooki desewr jyujyj weswede&nbsp;ju7j 8kiu8ooki desewr jyujyj weswede&nbsp;ju7j 8kiu8ooki desewr jyujyj weswede&nbsp;ju7j 8kiu8ooki desewr jyujyj weswede</p>', '2017-08-06', NULL, NULL),
(3, 'Mega Event 3', NULL, '<p>nyhnjh r4e45r4 ,l,k,kioliliok</p>', '2017-08-15', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `video_name` longtext COLLATE utf8mb4_unicode_ci,
  `cover_image` longtext COLLATE utf8mb4_unicode_ci,
  `gallery_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_image_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `type`, `video_name`, `cover_image`, `gallery_image`, `category_id`, `gallery_image_status`, `created_at`, `updated_at`) VALUES
(11, 0, NULL, NULL, '1505970132.jpg', '17', '1', NULL, NULL),
(12, 0, NULL, NULL, '1505970175.jpg', '16', '1', NULL, NULL),
(14, 0, NULL, NULL, '1506947786.jpg', '16', '1', NULL, NULL),
(15, 0, NULL, NULL, '1506947941.jpg', '16', '1', NULL, NULL),
(16, 0, NULL, NULL, '1506948565.jpg', '16', '1', NULL, NULL),
(17, 0, NULL, NULL, '1508074878.jpg', '16', '1', NULL, NULL),
(18, 0, NULL, NULL, '1510210718.jpg', '13', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_cats`
--

CREATE TABLE `gallery_cats` (
  `id` int(11) NOT NULL,
  `cat_name` longtext,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_cats`
--

INSERT INTO `gallery_cats` (`id`, `cat_name`, `status`) VALUES
(12, 'Nature', NULL),
(13, 'Photography', NULL),
(15, 'Bokeh', NULL),
(16, 'Stage decoration', NULL),
(17, 'Wedding photography', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hit_counter`
--

CREATE TABLE `hit_counter` (
  `id` int(11) NOT NULL,
  `counter` int(11) DEFAULT NULL,
  `daily_count` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hit_counter`
--

INSERT INTO `hit_counter` (`id`, `counter`, `daily_count`, `created_at`, `updated_at`) VALUES
(1, 9, 2, '2017-08-01 05:26:41', '17-08-01');

-- --------------------------------------------------------

--
-- Table structure for table `interior`
--

CREATE TABLE `interior` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `interior`
--

INSERT INTO `interior` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(3, 'Catering Package 1', '1502014346.jpg', '<p>This is where catering comes in. A good catering service, like ours, will provide</p>\r\n\r\n<ul>\r\n	<li>The delicious menu</li>\r\n	<li>Help with setting up</li>\r\n	<li>Arrange the dining area</li>\r\n	<li>Free Drink for Everyone</li>\r\n	<li>Serve the food efficiently</li>\r\n	<li>Take care to avoid any food related glitches</li>\r\n	<li>Serve as waiters and waitresses</li>\r\n	<li>Assist at the bar, if needed</li>\r\n	<li>Calculate the amount of food needed</li>\r\n	<li>Decide the sequence of dishes</li>\r\n	<li>Make sure the food tastes and looks good</li>\r\n	<li>Explain the exotic dishes to guests</li>\r\n</ul>', 6, NULL, '1', NULL, NULL),
(4, 'Wedding Package 1', '1502014301.jpg', '<p>It is amazing how the right lighting and sound systems can transform your event from a mundane, one among a thousand weddings to an ethereal, magical, fairy tale land where the beautiful princess ties the knot with her prince charming.</p>\r\n\r\n<p>Good and well placed lighting, we have found, can hide all the minor faults that can often make a wedding venue unattractive. It subtly highlights the more pleasant elements that are worth showing off. We always emphasize to our clients the fact of how important the proper lighting is for their marriage location.</p>', 5, NULL, '1', NULL, NULL),
(5, 'Wedding Package 2', '1502014251.jpg', '<p>Choosing the right wedding venue is important, as it can often be one of the things that makes or mars your special day. When the preliminaries are ever, and the D-Day has been decided, You, now, have some idea of when the event will take place, it&rsquo;s time to decide on the wedding venue!</p>\r\n\r\n<p>The process is complicated, and requires a fair amount of serious thought. It is not a simple matter of randomly deciding on the venue your best friend or neighbor tied the knot at. Each wedding, each couple, each event is different, so... take all your special needs into account while choosing the wedding venue</p>', 5, NULL, '1', NULL, NULL),
(6, 'Catering Package 2', '1502014407.jpg', '<p>Finding a wedding caterer who will not let you down is a lot to manage on your own. While no celebration of such a magnitude is ever complete if good food is not part of it, finding the right person or service to provide that food can make or mar the wedding for your guests. Unless the guests enjoy the food set out for them, their enjoyment of the event will be incomplete. Make the food and the service great, on the other hand, and it adds a dimension to their enjoyment, and they will be talking about it for a long time afterwards!</p>', 6, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `int_categories`
--

CREATE TABLE `int_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `int_categories`
--

INSERT INTO `int_categories` (`id`, `cat_name`, `cat_status`, `created_at`, `updated_at`) VALUES
(5, 'WEDDING PACKAGES', '1', NULL, NULL),
(6, 'CATERING PACKAGES', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `int_subcategories`
--

CREATE TABLE `int_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `int_subcategories`
--

INSERT INTO `int_subcategories` (`id`, `sub_cat_name`, `category_id`, `sub_cat_status`, `created_at`, `updated_at`) VALUES
(4, 'sub cat 1', 2, '1', NULL, NULL),
(5, 'sub cat 2', 3, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2017_07_17_113813_create_categories_table', 1),
(16, '2017_07_17_113838_create_subcategories_table', 1),
(17, '2017_07_17_113916_create_sliders_table', 1),
(18, '2017_07_17_113940_create_events_table', 1),
(19, '2017_07_17_113959_create_teams_table', 1),
(20, '2017_07_17_114018_create_reviews_table', 1),
(21, '2017_07_17_114045_create_galleries_table', 1),
(22, '2017_07_17_114101_create_abouts_table', 1),
(23, '2017_07_17_114121_create_services_table', 1),
(24, '2017_07_18_035856_create_eventcategories_table', 1),
(27, '2017_07_18_042138_create_packages_table', 2),
(28, '2017_07_18_042154_create_packagecategories_table', 2),
(29, '2017_07_18_055256_create_servicetypes_table', 3),
(30, '2014_02_09_225721_create_visitor_registry', 4);

-- --------------------------------------------------------

--
-- Table structure for table `music`
--

CREATE TABLE `music` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `music`
--

INSERT INTO `music` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(1, 'music 1', '1501179964.jpg', '<p>po8nuop;i</p>', NULL, NULL, '1', NULL, NULL),
(3, 'music 2', '1501180077.jpg', '<p>o8ujo8u nop9ou9 p009u0p9</p>', NULL, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `music_subcategories`
--

CREATE TABLE `music_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `offer_description` longtext,
  `offer_start` longtext,
  `offer_end` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `offer_description`, `offer_start`, `offer_end`, `created_at`, `updated_at`) VALUES
(2, 'Offer 1', '15 07 2017 12:00 am', '31 07 2017 12:00 am', NULL, NULL),
(3, 'Offer 2', '08 09 2017 12:00 am', '29 09 2017 12:00 am', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `package_category`
--

CREATE TABLE `package_category` (
  `id` int(11) NOT NULL,
  `package_title` longtext,
  `package_price` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `package_category`
--

INSERT INTO `package_category` (`id`, `package_title`, `package_price`) VALUES
(5, 'Silver', '150000'),
(6, 'Gold', '25000'),
(7, 'Plutinum', '50000'),
(9, 'Dala Kula', '4,000--20,000');

-- --------------------------------------------------------

--
-- Table structure for table `package_items`
--

CREATE TABLE `package_items` (
  `id` int(11) NOT NULL,
  `item` longtext,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `package_items`
--

INSERT INTO `package_items` (`id`, `item`, `cat_id`) VALUES
(3, 'item1-Gold', 6),
(5, 'New item', 7),
(11, 'New item 2', 5),
(12, 'New item 2', 5),
(13, 'New item 2', 5),
(14, 'New item 2', 5),
(15, 'New item Gold', 6),
(16, 'item2 Gold', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE `portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(20, 'HOLUD NIGHT', '1508076806.jpg', '<p>Holud&nbsp;night</p>\r\n\r\n<p>Stage Dacor ,D.J, Band party,<br />\r\n-Baul Singer. (বাউল সংগীত)<br />\r\n-Sound &amp; Light Arrangement.<br />\r\n-Elephant,Horse, Palki (পালকি) Tom-tom, Dhol, Shanai,Fireworks<br />\r\n-Back &amp; Front Screen Projector.<br />\r\n-Venue Arrangement.<br />\r\n-Food Arrangement<br />\r\n-Chotpoti, Fuckha (ফুচকা), Jeelapi, Coffee,Pitha, Candy floss, Pop corn, B.B.Q Items, Jhal-muri, Lassi and many more...</p>', NULL, 0, '1', NULL, NULL),
(21, 'wedding stage', '1508074205.jpg', '<h2>luxury Wedding Stage,<br />\r\ncomplete decoration plan<br />\r\nStage, Entry Gate, Table Top, Chair Bow, Table Centerpieces, After gate decor, Walkway Red Carpet, Photo Booth, Head Table Decor, Welcome board.<br />\r\nFor Price Inquiry Please Contact:</h2>\r\n\r\n<h2>01676662596, 01921351647</h2>\r\n\r\n<p>facebook;<a href=\"http://www.facebook.com/rongtuli.event\" target=\"_blank\">www.facebook.com/rongtuli.event</a>&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', NULL, 0, '1', NULL, NULL),
(22, 'BRIGHT & GROOM', '1508074727.jpg', '<p>beautiful Bright&nbsp;&amp;&nbsp;Groom&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>50%</strong>&nbsp;payment is required as<strong>&nbsp;booking money</strong>&nbsp;(minimum&nbsp;<strong>30 days</strong>&nbsp;before the event) &amp; the rest 50% is payable on the day of event or one week before delivery.&nbsp;<strong>(we do not accept cheques)</strong></li>\r\n	<li><strong>Rong Tuli&nbsp;</strong>&nbsp;holds the right to display any photograph and video to promote the business in Facebook, advertising media, brochures, magazine articles, websites, sample albums etc.</li>\r\n	<li>Booking money is non-refundable, but may rescheduled under special circumstances and on availability.</li>\r\n	<li>It may take approx.&nbsp;<strong>10 weeks</strong>&nbsp;to deliver the photos &amp; videos (prints &amp; DVDs), but u can take raw files after 6/7&nbsp;days of the event by paying rest of the amount.&nbsp;</li>\r\n	<li>For additional hour it will be charged&nbsp;<strong>3,000 BDT</strong>&nbsp;per hour.</li>\r\n	<li>All the Charges mentioned here are&nbsp;<strong>not negotiable</strong>&nbsp;and only applicable for the&nbsp;<strong>Events held in DHAKA</strong>.</li>\r\n	<li>For better Photographs and motion pic the location/venue/stages/places where the character/Bride &amp; Groom would been must have adequate light and proper decorations.</li>\r\n	<li><strong>Rong Tuli Wedding&nbsp;&amp; Event</strong>&nbsp;holds the right to change any of these package details.</li>\r\n	<li>Contact# +88 016 76 66 25 96,+88 019 21 35 16 47</li>\r\n	<li>Follow Us On inst&nbsp;<a href=\"http://www.instagram.com/rongtuli.event\" target=\"_blank\">www.instagram.com/rongtuli.event</a>&nbsp;</li>\r\n	<li>Facebook&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<a href=\"http://www.facebook.com/rongtuli.event\" target=\"_blank\">www.facebook.com/rongtuli.event</a></li>\r\n	<li>\r\n	<p><strong><strong>&gt; Basic Package&nbsp;:<strong>&nbsp;</strong></strong></strong></p>\r\n	</li>\r\n	<li>Photographers : Any chief photographer with 1 asst. photographer.</li>\r\n	<li>Duration : 4 hrs (approx)</li>\r\n	<li>Area &nbsp;:&nbsp;<strong>Stage Oriented</strong>.</li>\r\n	<li>Capture : as much as possible.</li>\r\n	<li>Printed copies : 5L-60 copies, 12L-2 copies</li>\r\n	<li>All pictures will be delivered on a DVD as well.</li>\r\n	<li>Cost :&nbsp;<strong>24,999 BDT</strong>&nbsp;(per event)</li>\r\n	<li>\r\n	<p>&nbsp;Combo 01</p>\r\n	</li>\r\n	<li>2 Photographer + 1 Cinematographer&nbsp;</li>\r\n	<li>Duration : 4 hrs (approx)</li>\r\n	<li>Area : Venue Oriented. Specially designed for small engagement or nikkah events.</li>\r\n	<li>Printed copies : 5L-40 copies, 12L-1 copy.</li>\r\n	<li>All pictures will be delivered on a DVD as well.</li>\r\n	<li>Cost :&nbsp;<strong>29,999 BDT</strong>&nbsp;(per event)</li>\r\n	<li>\r\n	<p>Combo 02 :&nbsp;</p>\r\n	</li>\r\n	<li>3 Photographer including</li>\r\n	<li>1/1.5 hours max + 1 Cinematographer</li>\r\n	<li>Duration : 5 hrs (approx)</li>\r\n	<li>Area : Venue Oriented.</li>\r\n	<li>Printed copies : 5L-60 copies, 12L-2 copy.</li>\r\n	<li>All pictures will be delivered on a DVD as well.</li>\r\n	<li>Cost :&nbsp;<strong>45,999 BDT</strong>&nbsp;(per event)</li>\r\n</ul>', NULL, 0, '1', NULL, NULL),
(23, 'Reception Stage', '1508078289.jpg', '<h2>luxury Reception&nbsp;Stage,<br />\r\ncomplete decoration plan<br />\r\nStage, Entry Gate, Table Top, Chair Bow, Table Centerpieces, After gate decor, Walkway Red Carpet, Photo Booth, Head Table Decor, Welcome board.<br />\r\nFor Price Inquiry Please Contact:</h2>\r\n\r\n<h2>01676662596, 01921351647</h2>\r\n\r\n<p>facebook;<a href=\"http://www.facebook.com/rongtuli.event\" target=\"_blank\">www.facebook.com/rongtuli.event</a>&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', NULL, 0, '1', NULL, NULL),
(24, 'Birth Day Stage', '1508078926.jpg', '<p>We Have Exclusive Birth Day Planning, Package it&#39;s make your baby&#39;s birth day too much memoribol and&nbsp;&nbsp;spasial in his life,it&#39;s our promic</p>\r\n\r\n<p>catuch us;<a href=\"http://www.facebook.com/rongtuli.event\" target=\"_blank\">www.facebook.com/rongtuli.event</a>&nbsp;&nbsp;</p>\r\n\r\n<p>phn;016 766 625 96,, 019 213 516 47&nbsp;</p>\r\n\r\n<p>&nbsp;</p>', NULL, 0, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `review_title`, `review_description`, `review_image`, `review_status`, `created_at`, `updated_at`) VALUES
(8, 'Review 4', '<p>This is a demo review 4</p>', '1500713667.jpg', '1', '22 July, 2017', NULL),
(9, 'Review 2', '<p>Demo&nbsp;review 2</p>', '1500713867.jpg', '1', '22 July, 2017', NULL),
(10, 'Review 1', '<p>trbtyhnyun 65u6n5u5u</p>', '1500714088.jpg', '1', '22 July, 2017', NULL),
(11, 'Review 3', '<p>tbt 6yh67 65yuh65yun 656un6un 77jmuyjmuy hjujymnu drfewr sdcse ki,lk,il&nbsp;</p>', '1500714250.jpg', '1', '22 July, 2017', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(15, 'Service name', '1505799851.jpg', '<p>We offer you an array of services for wedding event management and co-ordination while you can simply sit back and enjoy the celebrations and our expert team takes care of all the arrangements. We have an expansive database and personal relationships with many of Bangladeshi&#39;s best known vendors and key suppliers. So it easy for us to manage the best value of your money.</p>\r\n\r\n<p>BD Showbiz transforms all your dreams into reality with just one click, with just one thought, relieving you all of the burdens and tensions of organizing or managing wedding on your own. Like an architect or an interior designer who plans and shapes your dream house, we also plan and design your dream day- your wedding day.</p>\r\n\r\n<p>The entire event is managed with meticulous care by our team leaving you free to spend time with your near and dear ones. Our highly professional and experienced team ensures &#39;high value for money&#39; and gives the entire event a modern state of the art feel. We combine professionalism with personalized attention.</p>', 3, 3, '1', NULL, NULL),
(16, 'Service name', '1505799873.jpg', '<p>Red Elegance Event&rsquo;s Balloon Decorating Service is designed to help you turn your party into an unforgettable event. Whether you are entertaining a few children at home, organising a dinner at a restaurant or hotel, hosting a product launch for thousands, balloon decorations are guaranteed to put your guests in the party mood.</p>\r\n\r\n<p>Over the years we have specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons. Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Decorating Service, please contact us.</p>\r\n\r\n<p>Starting From 8000 bdt</p>', 3, 2, '1', NULL, NULL),
(17, 'Tomtom Service', '1505799920.jpg', '<p>description</p>', NULL, 0, '1', NULL, NULL),
(18, 'Service name', '1505799949.jpg', '<p>Demo</p>', NULL, 0, '1', NULL, NULL),
(19, 'Service name', '1505799969.jpg', '<p>ver the years we have specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons. Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Decorat</p>', NULL, 0, '1', NULL, NULL),
(21, 'Service name', '1505800026.jpg', '<p>Dala service</p>', NULL, 0, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `servicetypes`
--

CREATE TABLE `servicetypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `servicetypes`
--

INSERT INTO `servicetypes` (`id`, `type_name`, `created_at`, `updated_at`) VALUES
(1, 'Wedding', NULL, NULL),
(2, 'Birthday', NULL, NULL),
(3, 'Corporate', NULL, NULL),
(4, 'Ticketing', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `single`
--

CREATE TABLE `single` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slider_image`, `slider_title`, `slider_subtitle`, `slider_status`, `created_at`, `updated_at`) VALUES
(5, '1505897205.jpg', 'Wedding Holud Stage', NULL, '1', NULL, NULL),
(10, '1506524342.jpg', 'Bridal Mehandi Designs', NULL, '1', NULL, NULL),
(11, '1506956674.jpg', 'Wedding Table Top', NULL, '1', NULL, NULL),
(12, '1506956780.jpg', 'Outdoor Lamp', NULL, '1', NULL, NULL),
(13, '1506957036.jpg', 'Wedding Reception Stage', NULL, '1', NULL, NULL),
(14, '1506957134.jpg', 'Wedding Reception Stage', NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `sub_cat_name`, `sub_cat_status`, `service_type`, `created_at`, `updated_at`) VALUES
(3, 'sub cat 1', '1', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `team_title`, `team_description`, `team_contact`, `team_image`, `team_status`, `created_at`, `updated_at`) VALUES
(13, 'Anam Redwan', 'Photographer', NULL, '1505709864.jpg', '1', NULL, NULL),
(14, 'Rakibul Islam Himel', 'Photographer', NULL, '1505709882.jpg', '1', NULL, NULL),
(16, 'Nabila Jahan', 'CEO', NULL, '1505709904.jpg', '1', NULL, NULL),
(17, 'New Member', 'Trainee', NULL, '1505709803.jpg', '1', NULL, NULL),
(18, 'Ahmed Rumel', 'Event Photographer @  Rong Tuli Wedding & Event Management,', NULL, '1509985334.jpg', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$36UNCJVp0rA0UURa3leOiebSOKhgqjAticgs9BquGRH1uEv6LB/SS', 'ji2xi5P8kXUIXoYY6BQtBaTEqYFcae7tA7DSuggazgIoaZ9yiszWXtv5OFjx', NULL, NULL),
(2, 'Admin AGV', 'admin@agvcorp.biz', '$2y$10$IhzYjNrAxUQY6GlO4ZiAzuUtwquN7a.UB1ly2sA.QrFRESp6betX6', NULL, NULL, NULL),
(3, 'User', 'user@gmail.com', '$2y$10$.rSw4TumCpfMHdaVw7808u9quytqWV5v3MzZ06clWb9oZPc67cQu2', 'QeVAUwNyWhgQYmuv6ZG4COlbNc1ARlEmqJ1TTLUh7bkXUiBb3RilZuBkrz6j', NULL, NULL),
(4, 'Akash', 'akash@gmail.com', '$2y$10$yVMVbg/0ERbvhNQEiVI8TOVgUBa7sGAMYqzXPTbMw7UH.LuFvv0NS', NULL, NULL, NULL),
(6, 'Shadi Mubarak Admin', 'admin@shadimubarakbd.com', '$2y$10$DOtr/Yri61J672PqBA/05eC8E.Q8gb/KIRgHlPkDDloX8eWbHqz3C', 'NMnHvVE83GWG9zxAHHfiwbpdGtJKWGxhR3u5f6a2ddrlf68MJb91ud2NiV9q', NULL, NULL),
(7, 'RongTuli Admin', 'admin@rongtulievent.com', '$2y$10$wOWZcMX5Db0rgiOuUqvoReQGGIrqX3NXjzjEMUfcaT9/YSE4IWt2O', 'E5tlrSsIbqp1ZzCtyVwfYO4Kv7P2R5tierR76PYzkyXDj3CyCewY5V1OFfC2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `video_name` longtext COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `video_name`, `status`, `created_at`, `updated_at`) VALUES
(1, '59294.mp4', 1, '2017-07-31 13:10:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `counter` int(11) DEFAULT '0',
  `daily_count` int(11) DEFAULT '0',
  `dhaka` int(11) DEFAULT '0',
  `chittagong` int(11) DEFAULT '0',
  `barisal` int(11) DEFAULT '0',
  `khulna` int(11) DEFAULT '0',
  `mymensingh` int(11) DEFAULT '0',
  `rajshahi` int(11) DEFAULT '0',
  `rangpur` int(11) DEFAULT '0',
  `sylhet` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `counter`, `daily_count`, `dhaka`, `chittagong`, `barisal`, `khulna`, `mymensingh`, `rajshahi`, `rangpur`, `sylhet`, `created_at`, `updated_at`) VALUES
(1, 12, 5, 4, 2, 1, 1, 0, 0, 0, 0, NULL, '17-08-13');

-- --------------------------------------------------------

--
-- Table structure for table `visitor_registry`
--

CREATE TABLE `visitor_registry` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clicks` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitor_registry`
--

INSERT INTO `visitor_registry` (`id`, `ip`, `country`, `clicks`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', NULL, 1, '2017-07-30 05:45:56', '2017-07-30 05:45:56');

-- --------------------------------------------------------

--
-- Table structure for table `wedding`
--

CREATE TABLE `wedding` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `workgalleries`
--

CREATE TABLE `workgalleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `video_name` longtext COLLATE utf8mb4_unicode_ci,
  `cover_image` longtext COLLATE utf8mb4_unicode_ci,
  `gallery_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_image_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `workgalleries`
--

INSERT INTO `workgalleries` (`id`, `type`, `video_name`, `cover_image`, `gallery_image`, `category_id`, `gallery_image_status`, `created_at`, `updated_at`) VALUES
(77, 0, NULL, NULL, '1505806247.jpg', '30', '1', NULL, NULL),
(79, 1, '1505807667.mp4', 'cover-1505807667.jpg', NULL, '29', '1', NULL, NULL),
(80, 1, '1505812935.mp4', 'cover-1505812935.jpg', NULL, '30', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `work_gallery_cats`
--

CREATE TABLE `work_gallery_cats` (
  `id` int(11) NOT NULL,
  `cat_name` longtext,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `work_gallery_cats`
--

INSERT INTO `work_gallery_cats` (`id`, `cat_name`, `status`) VALUES
(29, 'Portrait', NULL),
(30, 'Landscape', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthday_categories`
--
ALTER TABLE `birthday_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthday_subcategories`
--
ALTER TABLE `birthday_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventcategories`
--
ALTER TABLE `eventcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_cats`
--
ALTER TABLE `gallery_cats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hit_counter`
--
ALTER TABLE `hit_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interior`
--
ALTER TABLE `interior`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `int_categories`
--
ALTER TABLE `int_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `int_subcategories`
--
ALTER TABLE `int_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `music`
--
ALTER TABLE `music`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `music_subcategories`
--
ALTER TABLE `music_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_category`
--
ALTER TABLE `package_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_items`
--
ALTER TABLE `package_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicetypes`
--
ALTER TABLE `servicetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `single`
--
ALTER TABLE `single`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor_registry`
--
ALTER TABLE `visitor_registry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wedding`
--
ALTER TABLE `wedding`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workgalleries`
--
ALTER TABLE `workgalleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `work_gallery_cats`
--
ALTER TABLE `work_gallery_cats`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `birthday_categories`
--
ALTER TABLE `birthday_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `birthday_subcategories`
--
ALTER TABLE `birthday_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `eventcategories`
--
ALTER TABLE `eventcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `gallery_cats`
--
ALTER TABLE `gallery_cats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `hit_counter`
--
ALTER TABLE `hit_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `interior`
--
ALTER TABLE `interior`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `int_categories`
--
ALTER TABLE `int_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `int_subcategories`
--
ALTER TABLE `int_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `music`
--
ALTER TABLE `music`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `music_subcategories`
--
ALTER TABLE `music_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `package_category`
--
ALTER TABLE `package_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `package_items`
--
ALTER TABLE `package_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `servicetypes`
--
ALTER TABLE `servicetypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `single`
--
ALTER TABLE `single`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `visitor_registry`
--
ALTER TABLE `visitor_registry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wedding`
--
ALTER TABLE `wedding`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `workgalleries`
--
ALTER TABLE `workgalleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `work_gallery_cats`
--
ALTER TABLE `work_gallery_cats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
