<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    
    public function home(Request $request)
    {




        $sliders = DB::table('sliders')->get();
        $services = DB::table('services')->get();
       
        $members = DB::table('teams')->get();
        $stories = DB::table('birthday')->get();

        $galleries = DB::table('galleries')->get();

        $offers = DB::table('blogs')->get();
        $reviews = DB::table('reviews')->get();

        $work_gallery_cats = DB::table('work_gallery_cats')->get();
        $workgalleries = DB::table('workgalleries')->get();
       
 // echo "<pre>";
 //         print_r($reviews);
 //         exit;
//return $reviews;
        return view('frontend.home',compact('sliders','services','members','offers','galleries','reviews','work_gallery_cats'));
    }

   

     public function about()
    {
        $abouts = DB::table('abouts')->get();
        $teams = DB::table('teams')->get();
        return view('frontend.rongtuli.about',compact('abouts','teams'));
    }     
    public function portfolio($id)
    {
        $portfolio = DB::table('portfolios')->where('id',$id)->first();
        return view('frontend.rongtuli.portfolio_details',compact('portfolio'));
    }    

    public function service($id)
    {
        $portfolio = DB::table('services')->where('id',$id)->first();
        return view('frontend.rongtuli.service_detail',compact('portfolio'));
    }    
   public function packages()
    {
        $categories = DB::table('package_category')->get();
        return view('frontend.rongtuli.packages',compact('categories'));
    }    


    public function gallery()
    {
         $gallery_cats = DB::table('gallery_cats')->get();
         //$galleries = DB::table('galleries')->get();
         //return count($galleries);
        return view('frontend.rongtuli.gallery',compact('gallery_cats'));
    }   

















  //   public function service_cat($id)
  //   {
  //       $services = DB::table('services')
  //       ->where('service_type_id',$id)
  //       ->get();

  //       $type = DB::table('servicetypes')
  //           ->where('id',$id)
  //           ->first();
  //       return view('frontend.service_cat',compact('services','type'));
  //   }    
    
  //   public function allpackage()
  //   {
  //       $packages = DB::table('packages')->get();
  //       return view('frontend.allpackage',compact('packages'));
  //   }  

    

  //   public function wedding_event($id)
  //   {
       
  //       $servic = DB::table('services')->where('id',$id)->get();
  //       $service = $servic[0];
  //       return view('frontend.singlewedding',compact('service'));
  //   }    

  //   public function birthday_cat()
  //   {
  //       $birthdays = DB::table('birthday')->get();
  //       return view('frontend.allbirthday',compact('birthdays'));
  //   }  
  // public function birthday($id)
  //   {
       
  //       $servic = DB::table('birthday')->where('id',$id)->get();
  //       $service = $servic[0];
  //       return view('frontend.singlebirthday',compact('service'));
  //   }    

  //    public function music_cat()
  //   {
  //       $musics = DB::table('music')->get();
  //       return view('frontend.allmusic',compact('musics'));
  //   }  
  // public function music($id)
  //   {
       
  //       $servic = DB::table('music')->where('id',$id)->get();
  //       $service = $servic[0];
  //       return view('frontend.singlemusic',compact('service'));
  //   }    


  // public function int($id)
  //   {
       
  //       $servic = DB::table('interior')->where('id',$id)->get();
  //       $service = $servic[0];
  //       return view('frontend.singleinterior',compact('service'));
  //   }    
  //   public function allint($id)
  //   {
       
  //       $ct = DB::table('int_categories')->where('id',$id)->get();
  //       $catid = $ct[0]->id;
  //       $catname = $ct[0]->cat_name;
  //       $ints = DB::table('interior')->where('service_sub_cat_id',$catid)->get();
        
  //       return view('frontend.allint',compact('ints','catname'));
  //   }    
  //    public function singlepackage($id)
  //   {
  //       $package = DB::table('packages')->where('id',$id)->first();
  //       return view('frontend.singlepackage',compact('package'));
  //   }     
  
  //   public function client_contact()
  //   {
  //       return view('frontend.contact_client');
  //   }    
    
  //   public function storecontact_client()
  //   {
  //       $filename = time().".jpg";

  //       Image::make(Input::file('contact_image'))->save('public/uploads/contact/'.$filename);

  //       DB::table('contacts')
  //       ->insert(
  //           [
  //               'contact_title' => Input::get('contact_title'),
  //               'contact_email' => Input::get('contact_phone'),
  //               'contact_phone' => Input::get('contact_phone'),
  //               'contact_image' => $filename,
  //               'contact_description' => Input::get('contact_description'),
  //           ]
  //           );
  //       return redirect()->back()->with('success','Message Sent Successfully');
  //   }    

    public function contact()
    {
        return view('frontend.rongtuli.contact');
    }    

     public function storecontact()
    {
        DB::table('contacts')
        ->insert(
            [
                'contact_title' => Input::get('contact_title'),
                'contact_email' => Input::get('contact_email'),
                'contact_phone' => Input::get('contact_phone'),
                'contact_description' => Input::get('contact_description'),
            ]
            );
        return redirect()->back()->with('success','Message Sent Successfully');
    }    
    


}
