<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'gt';
        $services = DB::table('interior')
                    ->leftjoin('int_categories','interior.service_sub_cat_id','=','int_categories.id')
                    ->select('interior.*','int_categories.cat_name')
                    ->get();
        return view('backend.package.interiors',compact('services'));
    }


    public function add()
    {
        $int_categories = DB::table('int_categories')->get();
        return view('backend.package.addinterior',compact('int_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgname = Input::get('name');
        $filename = time().'.jpg';

        // if(Input::has('name'))
        // {
        //     return "hy";
        //     
       
            
        //     //return $filename;
        // }
        Image::make(Input::file('name'))->save('public/uploads/interior/'.$filename);

        DB::table('interior')->insert(
        [
            'service_title' => Input::get('service_title'),
            'service_sub_cat_id' => Input::get('cat_id'),
            'service_image' => $filename,
            'service_description' => Input::get('editor1'),
            'service_status' => 1,
        ]
        );
         return redirect('packages')->with('success', 'New Package Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = DB::table('interior')
                    ->where('interior.id',$id)
                    ->leftjoin('int_categories','interior.service_sub_cat_id','=','int_categories.id')
                    ->select('interior.*','int_categories.cat_name')
                    ->get();
        $cats = DB::table('int_categories')->get();
        $service = $services[0];
        //return $services;
        return view('backend.package.editinterior',compact('service','cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('interior')
            ->where('id', $id)
            ->update([
            'service_title' => Input::get('service_title'),
            'service_description' => Input::get('editor1'),
            'service_status' => 1,
            'service_sub_cat_id' => Input::get('cat_id'),
                ]);
            if(Input::file('name'))
            {
                $item = DB::table('interior')
            ->where('id', $id)->first();
                $image = $item->service_image;

                unlink('public/uploads/interior/'.$image);
                //return 'hy';
                 $filename = time().'.jpg';

            Image::make(Input::file('name'))->save('public/uploads/interior/'.$filename);
            DB::table('interior')
            ->where('id', $id)
            ->update([
                    
                    'service_image' => $filename,
                    
                ]);

            }

            return redirect('packages')->with('success', ' Package Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //   Delete Image
        $item = DB::table('interior')
            ->where('id', $id)->first();
        $image = $item->service_image;
        unlink('public/uploads/interior/'.$image);
        

        DB::table('interior')->where('id', $id)->delete();

        return redirect('packages')->with('success', 'Selected  package removed Successfully');
    }
}
