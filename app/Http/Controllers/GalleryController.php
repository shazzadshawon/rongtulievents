<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = DB::table('galleries')
         ->get();
        return view('backend.gallery.galleries',compact('galleries'));
    }

 

    public function add()
    {
        $cats = DB::table('gallery_cats')->get();
        return view('backend.gallery.addgallery',compact('cats'));
    }

 public function store(Request $request)
    {
        $input = Input::all();
        //return Input::file('gallery_image');

        $imgname = Input::get('gallery_image');
        //$filename = time().'.jpg';
        $cat_id = Input::get('place');
        $dir = DB::table('gallery_cats')->where('id',$cat_id)->first();
//var_dump($dir->cat_name); exit;

        //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);


        $file = array_get($input,'gallery_image');
           // SET UPLOAD PATH 
            $destinationPath = 'public/uploads/gallery/'; 
            // GET THE FILE EXTENSION
            $extension = $file->getClientOriginalExtension(); 
            
            //  var_dump($extension) ;
            // exit;
            
            
            $fileName = time() . '.' . $extension; 
          
            Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$dir->cat_name.'/'.$fileName);

        
       

        DB::table('galleries')->insert(
        [
            'gallery_image' => $fileName,
            'gallery_image_status' => 1,
            'category_id' => $cat_id,
            'type' => 0,
        ]
        );
         return redirect('galleries')->with('success', 'New Image Uploaded Successfully');
    }

    public function storegallerycat(Request $request)
    {
        $exist = DB::table('gallery_cats')->where('cat_name',Input::get('cat_name'))->first();

        if(!empty($exist))
        {
            return redirect()->back()->with('success', ' Category Already Exist');
        }

        $cat_id = DB::table('gallery_cats')->insertGetId(
        [
            'cat_name' => Input::get('cat_name'),
           
        ]
        );
        $cat = DB::table('gallery_cats')->where('id',$cat_id)->first();
        //var_dump($cat->cat_name);exit;
        $result = \File::makeDirectory('public/uploads/gallery/'.$cat->cat_name, 0777, true);
         return redirect()->back()->with('success', 'New Category Added Successfully');
    }



    public function storevideo(Request $request)
    {
        $input = Input::all();
      
        $cat_id = Input::get('place');
        $dir = DB::table('gallery_cats')->where('id',$cat_id)->first();

        //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);

       

        $video = array_get($input,'video_name');
           
            $destinationPath = 'public/uploads/gallery/'.$dir->cat_name.'/';
          
            $extension1 = $video->getClientOriginalExtension(); 
            
            
            
        $cover = array_get($input,'cover_image');
          
            $extension2 = $cover->getClientOriginalExtension(); 
            // var_dump($extension2) ;
            // exit;
            if($extension2 !="jpg")
            {
                return redirect()->back()->with('danger','Cover Image Type Must be in jpg Format');
            }
            
            
            
            $name = time();
            $fileName =  $name . '.' . $extension1; 


 $video->move($destinationPath, $fileName); 

            $cover_image = 'cover-'.$name . '.' . $extension2; 
          

        $cover->move($destinationPath, $cover_image); 
       

        DB::table('galleries')->insert(
        [
            'video_name' => $fileName,
            'cover_image' => $cover_image,
            'gallery_image_status' => 1,
            'category_id' => $cat_id,
            'type' => 1,
        ]
        );
         return redirect('galleries')->with('success', 'New video Uploaded Successfully');
    }

   

    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        $gal = DB::table('galleries')
                    ->where('id',$id)
                    ->get();
                    $gallery = $gal[0];
        return view('backend.gallery.editgallery',compact('gallery'));
    }



    public function update(Request $request, $id)
    {
        $place = Input::get('place');
          if(Input::file('gallery_image'))
            {
                //return 'hy';
                 $filename = time().'.jpg';
Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);
                   

        // if($place == 'wedding'){
        //     //return 1;
            
        // }
        // elseif($place == 'events'){
        //     Image::make(Input::file('gallery_image'))->save('public/images/gallery/events/'.$filename);
        // }
        // elseif ($place == 'music') {
        //      Image::make(Input::file('gallery_image'))->save('public/images/gallery/music/'.$filename);
        // }


                 //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);

                   DB::table('galleries')
            ->where('id', $id)
            ->update([
                    
                    'gallery_image' => $filename,
                    'folder' => $place,
                    
                ]);


             //return redirect()->back()->with('success', 'Gallery Image Updated Successfully');
             }
             else{

                   DB::table('galleries')
            ->where('id', $id)
            ->update([
                    
                    
                    'folder' => $place,
                    
                ]);


             }
             return redirect()->back()->with('success', 'Gallery Image Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //return
         $gallery = DB::table('galleries')
            ->where('id', $id)->get();
 //$folder = $image[0]->folder;

            //$img = Image::destroy('public/images/gallery/'.$folder.'/'.$image[0]->gallery_image);

            $dir = DB::table('gallery_cats')->where('id',$gallery[0]->category_id)->first();
            if($gallery[0]->type=='0')
            {
                unlink('public/uploads/gallery/'.$dir->cat_name.'/'.$gallery[0]->gallery_image);

                DB::table('galleries')
                ->where('id', $id)->delete();
                return redirect('galleries')->with('success', ' Image Deleted Successfully');
            }
            else{
               unlink('public/uploads/gallery/'.$dir->cat_name.'/'.$gallery[0]->video_name); 
               unlink('public/uploads/gallery/'.$dir->cat_name.'/'.$gallery[0]->cover_image); 
                DB::table('galleries')
                ->where('id', $id)->delete();
                return redirect('galleries')->with('success', ' Video Deleted Successfully');
            }


        
    }

    public function gallerycat()
    {
         //return
         $gallery_cats = DB::table('gallery_cats')
            ->get();
 //$folder = $image[0]->folder;

          return view('backend/gallery/gallery_cats',compact('gallery_cats'));

        
    }



  public function deletegallerycat($id)
    {
         //return
         $gallerys = DB::table('galleries')
         ->where('category_id',$id)
            ->delete();

          $gallery_cats = DB::table('gallery_cats')
         ->where('id',$id)
            ->first();
            
            //unlink('public/uploads/gallery/'.$gallery_cats->cat_name);
            \File::deleteDirectory('public/uploads/gallery/'.$gallery_cats->cat_name);

            DB::table('gallery_cats')
         ->where('id',$id)
            ->delete();

          return redirect('gallerycat')->with('success', ' Category Deleted Successfully');

        
    }
}
