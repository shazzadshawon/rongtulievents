<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = DB::table('teams')
                    ->get();
        return view('backend.team.teams',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('backend.team.addteam');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgname = Input::get('name');
        $filename = time().'.jpg';

        // if(Input::has('name'))
        // {
        //     return "hy";
        //     
       
            
        //     //return $filename;
        // }
        Image::make(Input::file('team_image'))->save('public/uploads/team/'.$filename);

        DB::table('teams')->insert(
        [
            'team_title' => Input::get('team_title'),
            'team_image' => $filename,
            'team_description' => Input::get('team_description'),
            'team_contact' => Input::get('team_contact'),
            'team_status' => 1,
        ]
        );
         return redirect('teammembers')->with('success', 'New team member Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = DB::table('teams')
                    ->where('id',$id)
                    ->get();
        return view('backend.team.member',compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teams = DB::table('teams')
                   ->where('id',$id)
                   ->get();

      
        $team = $teams[0];
        //return $teams;
        return view('backend.team.editteam',compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //return Input::all();
         DB::table('teams')
            ->where('id', $id)
            ->update([
                     'team_title' => Input::get('team_title'),

                    'team_description' => Input::get('team_description'),
                    'team_contact' => Input::get('team_contact'),
                    'team_status' => 1,
                ]);
            if(Input::file('team_image'))
            {
                    $team = DB::table('teams')->where('id', $id)->first();
                    unlink('public/uploads/team/'.$team->team_image);
                    $filename = time().'.jpg';

                 Image::make(Input::file('team_image'))->save('public/uploads/team/'.$filename);
                   DB::table('teams')
            ->where('id', $id)
            ->update([
                    
                    'team_image' => $filename,
                    
                ]);

            }

            return redirect('teammembers')->with('success', 'member Info Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = DB::table('teams')->where('id', $id)->first();
        unlink('public/uploads/team/'.$team->team_image);

        DB::table('teams')->where('id', $id)->delete();


        return redirect('teammembers')->with('success', 'Member removed Successfully');
    }
}
