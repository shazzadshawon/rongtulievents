<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class WorkGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = DB::table('workgalleries')
         ->get();
        return view('backend.workgallery.workgalleries',compact('galleries'));
    }

 

    public function add()
    {
        $work_cats = DB::table('work_gallery_cats')->get();
        return view('backend.workgallery.addworkgallery',compact('work_cats'));
    }

 public function store(Request $request)
    {
        $input = Input::all();
        //return Input::file('gallery_image');

        $imgname = Input::get('gallery_image');
        //$filename = time().'.jpg';
        $cat_id = Input::get('place');
        $dir = DB::table('work_gallery_cats')->where('id',$cat_id)->first();
//var_dump($dir->cat_name); exit;

        //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);


        $file = array_get($input,'gallery_image');
           // SET UPLOAD PATH 
            $destinationPath = 'public/uploads/workgallery/'; 
            // GET THE FILE EXTENSION
            $extension = $file->getClientOriginalExtension(); 
            // RENAME THE UPLOAD WITH RANDOM NUMBER 
            $fileName = time() . '.' . $extension; 
            // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
            //$upload_success = $file->move($destinationPath, $fileName); 
            //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/work_cat_2/'.$fileName);
            Image::make(Input::file('gallery_image'))->save('public/uploads/workgallery/'.$dir->cat_name.'/'.$fileName);

        
       

        DB::table('workgalleries')->insert(
        [
            'gallery_image' => $fileName,
            'gallery_image_status' => 1,
            'category_id' => $cat_id,
            'type' => 0,
        ]
        );
         return redirect('workgalleries')->with('success', 'New Image Uploaded Successfully');
    }

    public function storeworkcat(Request $request)
    {
        $exist = DB::table('work_gallery_cats')->where('cat_name',Input::get('cat_name'))->first();

        if(!empty($exist))
        {
            return redirect()->back()->with('success', ' Category Already Exist');
        }

        $cat_id = DB::table('work_gallery_cats')->insertGetId(
        [
            'cat_name' => Input::get('cat_name'),
           
        ]
        );
        $cat = DB::table('work_gallery_cats')->where('id',$cat_id)->first();
        //var_dump($cat->cat_name);exit;
        $result = \File::makeDirectory('public/uploads/workgallery/'.$cat->cat_name, 0777, true);
         return redirect()->back()->with('success', 'New Category Added Successfully');
    }



    public function storevideo(Request $request)
    {
        $input = Input::all();
     
        $cat_id = Input::get('place');
        $dir = DB::table('work_gallery_cats')->where('id',$cat_id)->first();


        $video = array_get($input,'video_name');
           
            $destinationPath = 'public/uploads/workgallery/'.$dir->cat_name.'/';
           
            $extension1 = $video->getClientOriginalExtension(); 
           
        $cover = array_get($input,'cover_image');
          
            $extension2 = $cover->getClientOriginalExtension(); 
            
            if($extension2 !="jpg")
            {
                return redirect()->back()->with('danger','Cover Image Type Must be in jpg Format');
            }
            
           
           
            $name = time();
            
            $fileName =  $name . '.' . $extension1; 
            $video->move($destinationPath, $fileName); 

        
            $cover_image = 'cover-'.$name . '.' . $extension2; 
            $cover->move($destinationPath, $cover_image); 
       

        DB::table('workgalleries')->insert(
        [
            'video_name' => $fileName,
            'cover_image' => $cover_image,
            'gallery_image_status' => 1,
            'category_id' => $cat_id,
            'type' => 1,
        ]
        );
         return redirect('workgalleries')->with('success', 'New video Uploaded Successfully');
    }

   

    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        $gal = DB::table('workgalleries')
                    ->where('id',$id)
                    ->get();
                    $gallery = $gal[0];
        return view('backend.workgallery.editworkgallery',compact('gallery'));
    }



//     public function update(Request $request, $id)
//     {
//         $place = Input::get('place');
//           if(Input::file('gallery_image'))
//             {
//                 //return 'hy';
//                  $filename = time().'.jpg';
// Image::make(Input::file('gallery_image'))->save('public/uploads/workgallery/'.$filename);
                   

//         // if($place == 'wedding'){
//         //     //return 1;
            
//         // }
//         // elseif($place == 'events'){
//         //     Image::make(Input::file('gallery_image'))->save('public/images/gallery/events/'.$filename);
//         // }
//         // elseif ($place == 'music') {
//         //      Image::make(Input::file('gallery_image'))->save('public/images/gallery/music/'.$filename);
//         // }


//                  //Image::make(Input::file('gallery_image'))->save('public/uploads/gallery/'.$filename);

//                    DB::table('workgalleries')
//             ->where('id', $id)
//             ->update([
                    
//                     'gallery_image' => $filename,
//                     'folder' => $place,
                    
//                 ]);


//              //return redirect()->back()->with('success', 'Gallery Image Updated Successfully');
//              }
//              else{

//                    DB::table('workgalleries')
//             ->where('id', $id)
//             ->update([
                    
                    
//                     'folder' => $place,
                    
//                 ]);


//              }
//              return redirect()->back()->with('success', 'Gallery Image Updated Successfully');
//     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //return
         $gallery = DB::table('workgalleries')
            ->where('id', $id)->get();
 //$folder = $image[0]->folder;

            //$img = Image::destroy('public/images/gallery/'.$folder.'/'.$image[0]->gallery_image);

            //$img->destroy();
            $dir = DB::table('work_gallery_cats')->where('id',$gallery[0]->category_id)->first();
            if($gallery[0]->type=='0')
            {
                unlink('public/uploads/workgallery/'.$dir->cat_name.'/'.$gallery[0]->gallery_image);

                DB::table('workgalleries')
                ->where('id', $id)->delete();
                return redirect('workgalleries')->with('success', ' Image Deleted Successfully');
            }
            else{
               unlink('public/uploads/workgallery/'.$dir->cat_name.'/'.$gallery[0]->video_name); 
               unlink('public/uploads/workgallery/'.$dir->cat_name.'/'.$gallery[0]->cover_image); 
                DB::table('workgalleries')
                ->where('id', $id)->delete();
                return redirect('workgalleries')->with('success', ' Video Deleted Successfully');
            }


        
    }




public function workgallerycat()
    {
         //return
         $work_gallery_cats = DB::table('work_gallery_cats')
            ->get();
 //$folder = $image[0]->folder;

          return view('backend/workgallery/work_gallery_cats',compact('work_gallery_cats'));

        
    }



  public function deleteworkgallerycat($id)
    {
         //return
         $gallerys = DB::table('workgalleries')
         ->where('category_id',$id)
            ->delete();

          $gallery_cats = DB::table('work_gallery_cats')
         ->where('id',$id)
            ->first();
            
            //unlink('public/uploads/gallery/'.$gallery_cats->cat_name);
            \File::deleteDirectory('public/uploads/workgallery/'.$gallery_cats->cat_name);

            DB::table('work_gallery_cats')
         ->where('id',$id)
            ->delete();

          return redirect('workgallerycat')->with('success', ' Category Deleted Successfully');

        
    }









}
