<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = DB::table('offers')
         ->get();
        return view('backend.offer.offers',compact('offers'));
    }

   

    public function add()
    {
        return view('backend.offer.addoffer');
    }


    public function store(Request $request)
    {
        //return 'hy';
        DB::table('offers')->insert(
        [
            'offer_description' => Input::get('offer_description'),
            'offer_start' => date('d m Y h:i a', strtotime(Input::get('offer_start'))),
            'offer_end' => date('d m Y h:i a', strtotime(Input::get('offer_end'))),
        ]
        );
         return redirect('offers')->with('success', 'New data Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {

        $offer = DB::table('offers')
        ->where('id',$id)
        ->first();
        //return $cat;
        return view('backend.offer.editoffer',compact('offer'));
    }


    public function update(Request $request, $id)
    {
        //return Input::get('offer_start');

        DB::table('offers')
            ->where('id', $id)
            ->update([
                     'offer_description' => Input::get('offer_description'),
                     'offer_start' => date('d m Y h:i a', strtotime(Input::get('offer_start'))),
                     'offer_end' => date('d m Y h:i a', strtotime(Input::get('offer_end'))),
                ]);
            return redirect('offers')->with('success', 'Offer Updated Successfully');


    }

  

    public function destroy($id)
    {
       

        DB::table('offers')->where('id', $id)->delete();
       

        return redirect('offers')->with('success', 'Data Deleted Successfully');
    }
}
