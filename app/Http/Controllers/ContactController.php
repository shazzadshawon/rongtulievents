<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class ContactController extends Controller
{
  

    public function index()
    {
        $contacts = DB::table('contacts')
         ->get();
        return view('backend.contact.contacts',compact('contacts'));
    }

    

    // public function add()
    // {
    //     return view('backend.about.addabout');
    // }


    // public function store(Request $request)
    // {
    //     //return 'hy';
    //     DB::table('contacts')->insert(
    //     [
    //         ' _description' => Input::get('editor1'),
    //     ]
    //     );
    //      return redirect('contacts')->with('success', 'New data Added Successfully');
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit($id)
    // {

    //     $about = DB::table('contacts')
    //     ->where('id',$id)
    //     ->first();
    //     //return $cat;
    //     return view('backend.about.editabout',compact('about'));
    // }


    // public function update(Request $request, $id)
    // {
    //     //return Input::all();

    //     DB::table('contacts')
    //         ->where('id', $id)
    //         ->update([
    //                  'about_description' => Input::get('editor1'),
                     
    //             ]);

    //         return redirect('contacts')->with('success', 'Data Updated Successfully');


    // }

  

    public function destroy($id)
    {
       

        DB::table('contacts')->where('id', $id)->delete();
       

        return redirect('messages')->with('success', 'Messege Deleted Successfully');
    }
}
