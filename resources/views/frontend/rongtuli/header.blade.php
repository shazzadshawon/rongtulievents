<div class="navbar">
    <div class="navbar-header">
        <div class="basic-wrapper">
            <div class="navbar-brand">
                <a href="{{url('/')}}">
                    <img src="#" srcset="style/images/rontuli-logo_e_h.png 1x, style/images/rontuli-logo_e_h@2x.png 2x" class="logo-light" alt="" />
<!--                    <img src="#" srcset="style/images/logo-dark.png 1x, style/images/logo-dark@2x.png 2x" class="logo-dark" alt="" />-->
                </a>
            </div>
            <a class="btn responsive-menu" data-toggle="collapse" data-target=".navbar-collapse"><i></i></a>
        </div>
        <!-- /.basic-wrapper -->
    </div>
    <!-- /.navbar-header -->
    <nav class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="current"><a href="{{url('/')}}">Home</a></li>
            <li class="dropdown"><a href="#" class="dropdown-toggle js-activated">Portfolio <span class="caret"></span></a>
            @php
            $portfolios = DB::table('portfolios')->get();
            @endphp
                <ul class="dropdown-menu">
                @foreach($portfolios as $portfolio)
                    <li><a href="{{url('portfolio/'.$portfolio->id)}}">{{$portfolio->service_title}}</a></li>
                @endforeach
                </ul>
            </li>
            <li><a href="{{url('gallery')}}">Gallery</a></li>
            <li><a href="{{url('about')}}">About</a></li>
            <li><a href="{{url('packages')}}">Packages</a></li>
            <li><a href="{{url('contact')}}">Contact Us</a></li>
        </ul>
        <!-- /.navbar-nav -->
    </nav>
    <!-- /.navbar-collapse -->
    <div class="social-wrapper">
        <ul class="social naked">
            <li><a href="http://www.facebook.com/rongtuli.event" target="_blank"><i class="icon-s-facebook"></i></a></li>
            <li><a href="https://twitter.com/rongtuli14" target="_blank"><i class="icon-s-twitter"></i></a></li>
            <li><a href="http://www.linkedin.com/in/rong-tuli-event-management-0a7a1814a" target="_blank"><i class="icon-s-linkedin"></i></a></li>
            <li><a href="http://www.instagram.com/rongtuli.event" target="_blank"><i class="icon-s-instagram"></i></a></li>
        </ul>
        <!-- /.social -->
    </div>
    <!-- /.social-wrapper -->
</div>