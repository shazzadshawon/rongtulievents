@extends('layouts.frontend')

@section('content')

            <div class="post-parallax parallax inverse-wrapper parallax1" style="background-image: url(style/images/art/parallax1.jpg);">
                <div class="container inner text-center">
                    <div class="headline text-center">
                        <h2>hello! we are RongTuli</h2>
                        <p class="lead">a bunch of super-heroes  working together to make your event  colorful,lively and full of fun </p>
                    </div>
                    <!-- /.headline -->
                </div>
                <!--/.container -->
            </div>
            <!--/.parallax -->


            <div class="light-wrapper">
                <div class="container inner">
                    <div class="row">
                        <div class="col-sm-5">
                            <figure><img src="style/images/service_slider/item_(1).jpg" alt="" /></figure>
                        </div>
                        <!--/column -->
                        <div class="col-sm-7">
                            <h3 class="section-title">About the company</h3>
                            <p>The team of RongTuli wedding & Event Management is a lot like a bunch of super-heroes  working together to make your event  colorful,lively and full of fun activities,our team has about over 40 member who work day and night so that, most special day of your life can be turned into a day that will be encrusted in the memory of not only you but also for many people.</p>
                            <p>We have 20 stage decoration designer,5 photographers,5 cinematographers,5 public relationship manager,5 supervisor. and a number of mehendiArtists,dj we work together as a team as well as individually to bring out the uniqueness that every event requires to shin-through.we hope to delight and dazzle for as long as we serve in the endless try.  </p>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                    <div class="divide60"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Why Choose Us?</h3>
                            <div class="divide5"></div>
                            <div class="panel-group" id="accordion">
                            @php
                            $i=0;
                            @endphp
                            @foreach($abouts as $about)
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"> <a data-toggle="collapse" class="panel-toggle @if($i==0) active @endif" data-parent="#accordion" href="#collapse{{$about->id}}">
                                        @php
                                            print_r($about->about_title);
                                        @endphp
                                        </a> </h4>
                                    </div>
                                    <div id="collapse{{$about->id}}" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                        @php
                                            print_r($about->about_description);
                                        @endphp
                                        </div>
                                    </div>
                                </div>
                                @php
                                $i++;
                                @endphp
                            @endforeach
                               
                            </div>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!--/.container -->
            </div>
            <!-- /.light-wrapper -->


            <div style="background: #e8e8e8; color: black;">
                <div class="container inner">
                    <div class="row">
                        <div class="col-sm-5">
                            <figure><img src="style/images/massegespicture.JPG" alt="" /></figure>
                        </div>
                        <!--/column -->
                        <div class="col-sm-7">
                            <h3 class="section-title" style="color: black">About the company</h3>
                            <p>It was 2013 when I looking that  i love to doing any wedding or event arrangement.i just love  to do that, and its make me happy,After few month i start my won wedding farm I struggle hard,to knowing everything about this,than i start small wedding,eventprogram,day by day now, Alhamdurellah i have lots of experiences,my past life made me stronger and understand the vision and mission of my wide base of clientele so that their events are always unique in representing them. I always try to understand my client base and their needs as it is my sole responsibility to make sure that your special event defines and reflects the essence of your personality all the way through. I have always focused on client satisfaction and made surethat
                                the relationship my beloved company builds with our clients is no less than a blissful marriage itself. The team of RongTuli Wedding & Events is my family and to me my family's happiness comes first. The quality that we maintain in our services is always impeccable and that is why in only 5 years of time RongTuli Wedding &Events  is standing strong successfully in front of so many. We have a number of peoples following us on our Facebook page who are always with us virtually, peeking into our world and supporting us all the way through.
                            </p>
                            <p>My dream is to paint your dreams through our work just to start off and then comes all the other elements that is needed for completing all your dreams successfully. I wish and hope to keep my company's reputation intact so that we keep on building many more dreams in the future while we hold our hands tightly. </p>
                        </div>
                        <!--/column -->
                    </div>
                </div>
                <!--/.container -->
            </div>
            <!-- /.light-wrapper -->


            <div class="dark-wrapper">
                <div class="container inner">
                    <h3 class="section-title text-center">Meet Our Team</h3>

                    <div class="carousel-wrapper">
                        <div class="carousel clients">
                        @foreach($teams as $team)
                            <div class="item text-center">
                                <figure><img style="height: 200px" src="{{asset('public/uploads/team/'.$team->team_image)}}" alt="" /></figure>
                                <h4 class="post-title">{{$team->team_title}}</h4>
                                <div class="meta"></div>
                                <p>@php print_r($team->team_description); @endphp</p>
                                <!-- <ul class="social naked bigger text-center">
                                    <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                    <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-s-pinterest"></i></a></li>
                                </ul> -->
                            </div>
                        @endforeach
                           

                          
                        </div>
                        <!--/.carousel -->
                    </div>
                    <!--/.carousel-wrapper -->
                </div>
                <!--/.container -->
            </div>

@endsection