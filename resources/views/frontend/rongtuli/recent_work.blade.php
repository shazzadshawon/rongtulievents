<div class="dark-wrapper">
    <div class="container inner">
        <div class="thin">
            <h3 class="section-title text-center">Recent Works</h3>
            <p class="text-center">Recent Work related text. Recent Work related text. Recent Work related text. Recent Work related text. Recent Work related text. </p>
        </div>
        <!-- /.thin -->
        <div class="divide10"></div>
        <div class="cbp-panel">
            <div id="js-filters-mosaic" class="cbp-filter-container text-center recent_work_header">
                <div data-filter="*" class="cbp-filter-item-active cbp-filter-item all"> All </div>
                @foreach($work_gallery_cats as $cat)
                <div data-filter=".{{$cat->cat_name}}" class="cbp-filter-item"> {{$cat->cat_name}} </div>
                @endforeach
               <!--  <div data-filter=".Events" class="cbp-filter-item"> Events </div>
                <div data-filter=".Landscape" class="cbp-filter-item"> Landscape </div>
                <div data-filter=".wedding" class="cbp-filter-item"> wedding</div> -->
                
            </div>

            <div id="js-grid-mosaic" class="cbp"></div>
            <!--/.cbp -->

        </div>
        <!--/.cbp-panel -->
        <div class="divide30"></div>
       <!--  <div id="js-grid-mosaic-more" class="cbp-l-loadMore-text"> <a href="ajax/loadmore.html" class="cbp-l-loadMorelink btn" rel="nofollow"> <span class="cbp-l-loadMore-defaultText">Load More</span> <span class="cbp-l-loadMore-loadingText">Loading...</span> <span class="cbp-l-loadMore-noMoreLoading">No More Works</span> </a> </div> -->
    </div>
    <!-- /.container -->
</div>
<!-- /.dark-wrapper -->

<script type="text/javascript">
    function readHeader(){
        var header = $('.recent_work_header').children();
        return header;
    }
    var index = 1;  

    function loadTargetElement(target){
        console.log('target');
        var orgWidth = 0;
        var orgHeight = 0;
        var folder = "public/uploads/workgallery/"+target+"/";
        $.ajax({
            url : folder,
            success: function (data) {
                $(data).find("a").attr("href", function (i, val) {
                    if( val.match(/\.(jpe?g|png)$/) ) {
                        var temp = val.split('/');
                        var temp2 = temp[temp.length-1];
                        if(temp2.split('-')[0] != "cover"){
                            console.log(temp2);
                            var src = val;
                            var element = '<div class="cbp-item '+target+'">'+
                                '<a class="cbp-caption fancybox-media cbp-lightbox" data-rel="portfolio" href="'+src+'" data-title-id="title-'+index+'" data-cbp-lightbox="recentWorkLightbox">' +
                                '<div class="cbp-caption-defaultWrap"><img src="'+src+'" alt="" style="width: 400px;" /></div>' +
                                '<div class="cbp-caption-activeWrap">' +
                                '<div class="cbp-l-caption-alignCenter">' +
                                '<div class="cbp-l-caption-body"><div class="cbp-l-caption-title"><span class="cbp-plus"></span></div></div>' +
                                '</div>' +
                                '</div>' +
                                '</a>' +
                                '</div>';
                            $('#js-grid-mosaic').append(element);
                        }
                    } else if( val.match(/\.(mp4|3gp)$/) ) {
                        var src =  val;
                         var temp = val.split('/');
                        var temp2 = temp[temp.length-1];
                        var vid_n = temp2.split('.')[0];

                        var cover_n = folder+'cover-'+vid_n+'.jpg';
                        var element_video = '<div class="cbp-item '+target+' motion">' +
                                                '<a class="cbp-caption fancybox-media cbp-lightbox" data-rel="portfolio" href="'+src+'" data-title-id="title-'+index+'" data-cbp-lightbox="recentWorkLightbox">' +
                                                    '<div class="cbp-caption-defaultWrap"> ' +
                                                        '<img src="'+cover_n+'" alt="" /> ' +
                                                    '</div>' +
                                                    '<div class="cbp-caption-activeWrap"> ' +
                                                        '<div class="cbp-l-caption-alignCenter"> ' +
                                                            '<div class="cbp-l-caption-body"> ' +
                                                                '<div class="cbp-l-caption-title"><span class="cbp-play"></span></div> ' +
                                                            '</div> ' +
                                                        '</div> ' +
                                                    '</div>' +
                                                    '<!--/.cbp-caption-activeWrap --> ' +
                                                '</a> ' +
                                                '<div id="title-1" class="hidden"> ' +
                                                    '<h3>Header</h3> ' +
                                                    '<p>Description Text Description Text Description Text Description Text </p> ' +
                                                '</div>' +
                                                '<!-- /.hidden -->' +
                                            '</div>';
                        $('#js-grid-mosaic').append(element_video);
                    }
                });
            }
        });
        index++;
    }

    $(document).ready(function(){
        var steps = readHeader();

        for(var i = 0; i < steps.length; i++){
            if(!$(steps[i]).hasClass('all')){
                var t= $(steps[i]).attr('data-filter');
                var target = t.split('.')[1];
                loadTargetElement(target);
            }
        }
    });

    $(window).load(function(){
        $('#js-grid-mosaic').cubeportfolio({
            filters: '#js-filters-mosaic',
            loadMore: '#js-grid-mosaic-more',
            loadMoreAction: 'click',
            layoutMode: 'mosaic',
            sortToPreventGaps: true,
            defaultFilter: '*',
            animationType: 'unfold',
            gapHorizontal: 0,
            gapVertical: 0,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 768,
                cols: 4
            }, {
                width: 767,
                cols: 2
            }],
            caption: 'fadeIn',
            displayType: 'fadeInToTop',
            displayTypeSpeed: 100,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">1 of 1</div>',
            // singlePageInline
            singlePageInlineDelegate: '.cbp-singlePageInline',
            singlePageInlinePosition: 'top',
            singlePageDeeplinking: true,
            singlePageInlineInFocus: true,
            offsetValue: 100,
            singlePageInlineCallback: function(url, element) {
                // to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)
                var t = this;
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 10000
                }).done(function(result) {
                    t.updateSinglePageInline(result);
                }).fail(function() {
                    t.updateSinglePageInline('AJAX Error! Please refresh the page!');
                });
            }
        });
    });
</script>