<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<footer class="footer inverse-wrapper">
    <div class="container inner">
        <div class="row">
            <div class="col-sm-4">
                <div class="widget">
                    <img class="img-responsive" src="style/images/rontuli-logo_e.png" alt="" style="width: 250px;">
                </div>
                <!-- /.widget -->
                <div class="widget">
                    <h4 class="widget-title deepRed">Get In Touch</h4>
<!--                    <p>COMPLETE WEDDING DECORATION PLAN Stage, Entry Gate, Table Top, Chair Bow, Table Centerpieces, After gate decor, Walkway Red Carpet, Photo Booth, Head Table Decor, Welcome board. For Booking Please Contact: 0167-6662596, 0187-9001862</p>-->
                    <div class="contact-info"> <i class="icon-location"></i> Chandonachowrasta ,bagdadTanzia tower, 2nd floor, tangail road, Gazipur, Dhaka, Bangladesh
                        <br />
                        <i class="icon-phone"></i>+88 019 21351647  <br />
                        <i class="icon-phone"></i>+88 016 76662596   <br />
                        <!--<i class="icon-phone"></i>+88 016 11365640  <br />-->
                        <i class="icon-mail"></i> <a href="mailto:contact@rangtulievents.com">contact@rangtulievents.com</a> </div>
                </div>
                <!-- /.widget -->

            </div>
            <?php 
            $services = DB::table('services')->orderby('id','desc')->take(3)->get();
             ?>

            <div class="col-sm-4">
                <div class="widget">
                    <h3 class="widget-title deepRed">Recently Added Services</h3>
                    <ul class="post-list lightRed">
                    <?php foreach ($services as $service): ?>
                       <?php if (!empty($service)): ?>
                            <li>
                            <div class="icon-overlay"> <a href="{{url('service/'.$service->id)}}"><img src="{{asset('public/uploads/service/'.$service->service_image)}}" alt="" /> </a> </div>
                            <div class="meta">
                                <h5><a class="lightRed" href="{{url('service/'.$service->id)}}">{{$service->service_title}}</a></h5>
                                <!-- <em style="color: red;"><span class="date">3th Oct 2012</span> <span class="comments"><a href="#"><i class="icon-chat-1"></i> 8</a></span></em>  --></div>
                        </li>
                       <?php endif ?>
                    <?php endforeach ?>
                        
                       
                    </ul>
                    <!-- /.post-list -->
                </div>
                <!-- /.widget -->
            </div>
            <!-- /column -->


            <div class="col-sm-4">
            <br><br>
                <!-- <div class="widget">
                    <h4 class="widget-title deepRed">Search</h4>
                    <form class="searchform" method="get">
                        <input type="text" id="s2" name="s" value="Search something" onfocus="this.value=''" onblur="this.value='Search something'">
                        <button type="submit" class="btn btn-default">Find</button>
                    </form>
                </div> -->
                <!-- /.widget -->
                <div class="widget">
                    <h4 class="widget-title deepRed">Find us</h4>
                    <ul class="social">
                        <li><a href="#"><i class="icon-s-rss"></i></a></li>
                        <li><a href="http://www.facebook.com/rongtuli.event" target="_blank"><i class="icon-s-facebook"></i></a></li>
                        <li><a href="https://twitter.com/rongtuli14" target="_blank"><i class="icon-s-twitter"></i></a></li>
                        <li><a href="http://www.linkedin.com/in/rong-tuli-event-management-0a7a1814a" target="_blank"><i class="icon-s-linkedin"></i></a></li>
                        <li><a href="http://www.instagram.com/rongtuli.event" target="_blank"><i class="icon-s-instagram"></i></a></li>
                    </ul>
                    <!-- .social -->

                </div>
            </div>
            <!-- /column -->
        </div>
        <!-- /.row -->
    </div>
    <!-- .container -->

    <div class="sub-footer">
        <div class="container inner">
            <h5 class="text-center"><strong> <span style="color:#ffffff">Created with <i class="fa fa-heart" style="color:purple" aria-hidden="true"></i> By</span> <a class="deepRed" href="http://shobarjonnoweb.com/">Shobarjonnoweb.com</a></strong></h5>
<!--            <p class="text-center">© 2015 Lydia. All rights reserved. Theme by <a href="http://elemisfreebies.com">elemis</a>.</p>-->
        </div>
        <!-- .container -->
    </div>
    <!-- .sub-footer -->
</footer>