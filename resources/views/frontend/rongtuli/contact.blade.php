@extends('layouts.frontend')

@section('content')

            <div id="map"></div>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACAEcQkWZJGNAZ3JunjlQcltX5LUtfLEQ&amp;sensor=false&amp;extension=.js"></script>
            <script> google.maps.event.addDomListener(window, 'load', init);
                var map;
                function init() {
                    var mapOptions = {
                        center: new google.maps.LatLng(23.9901, 90.37806),
                        zoom: 15,
                        zoomControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.DEFAULT,
                        },
                        disableDoubleClickZoom: false,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                        },
                        scaleControl: true,
                        scrollwheel: false,
                        streetViewControl: true,
                        draggable : true,
                        overviewMapControl: false,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        styles: [{stylers:[{saturation:-100},{gamma:1}]},{elementType:"labels.text.stroke",stylers:[{visibility:"off"}]},{featureType:"poi.business",elementType:"labels.text",stylers:[{visibility:"off"}]},{featureType:"poi.business",elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"poi.place_of_worship",elementType:"labels.text",stylers:[{visibility:"off"}]},{featureType:"poi.place_of_worship",elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"road",elementType:"geometry",stylers:[{visibility:"simplified"}]},{featureType:"water",stylers:[{visibility:"on"},{saturation:50},{gamma:0},{hue:"#50a5d1"}]},{featureType:"administrative.neighborhood",elementType:"labels.text.fill",stylers:[{color:"#333333"}]},{featureType:"road.local",elementType:"labels.text",stylers:[{weight:0.5},{color:"#333333"}]},{featureType:"transit.station",elementType:"labels.icon",stylers:[{gamma:1},{saturation:50}]}]
                    }

                    var mapElement = document.getElementById('map');
                    var map = new google.maps.Map(mapElement, mapOptions);
                    var locations = [
                        ['Rong Tuli Events Ltd. Bagdad Tanzia Tower', 23.9901, 90.37806]
                    ];
                    for (i = 0; i < locations.length; i++) {
                        marker = new google.maps.Marker({
                            icon: 'style/images/map-pin.png',
                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                            map: map
                        });
                    }
                }
            </script>
            <div style="background: purple">
                <div class="container inner">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2 class="section-title">Get in Touch</h2>
                            <p>Complete Wedding Decoration plan, Stage, Entry Gate, Table Top, Chair Bow, Table Centerpieces, After gate decor, Walkway Red Carpet, Photo Booth, Head Table Decor, Welcome board. </p>
                            <div class="divide10"></div>
                            <div class="form-container">
                                <form action="{{url('storecontact')}}" method="post" >
                                {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-field">
                                                <label>
                                                    <input type="text" name="contact_title" placeholder="Your name" required="required">
                                                </label>
                                            </div>
                                            <!--/.form-field -->
                                        </div>
                                        <!--/column -->
                                        <div class="col-sm-6">
                                            <div class="form-field">
                                                <label>
                                                    <input type="email" name="contact_email" placeholder="Your e-mail" required="required">
                                                </label>
                                            </div>
                                            <!--/.form-field -->
                                        </div>
                                        <!--/column -->
                                        <div class="col-sm-12">
                                            <div class="form-field">
                                                <label>
                                                    <input type="tel" name="contact_phone" placeholder="Phone">
                                                </label>
                                            </div>
                                            <!--/.form-field -->
                                        </div>
                                        <!--/column -->
                                       <!--  <div class="col-sm-6">
                                            <div class="form-field">
                                                <label class="custom-select">
                                                    <select name="department" required="required">
                                                        <option value="">Select Department</option>
                                                        <option value="Sales">Sales</option>
                                                        <option value="Support">Customer Support</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                    <span></span> </label>
                                            </div>
                                         
                                        </div> -->
                                        <!--/column -->
                                    </div>
                                    <!--/.row -->
                                    <textarea name="contact_description" placeholder="Type your message here..." required="required"></textarea>
                                   <!--  -->
                                    <!--/.radio-set -->
                                    <input type="submit" class="btn" value="Send" >
                                    <footer class="notification-box"></footer>
                                </form>
                                <!--/.vanilla-form -->
                            </div>
                            <!--/.form-container -->

                        </div>
                        <!--/column -->

                        <aside class="col-sm-4">
                            <div class="sidebox widget">
                                <h3 class="widget-title">Address</h3>
<!--                                <p>Fusce dapibus, tellus commodo, tortor mauris condimentum utellus fermentum, porta sem malesuada magna. Sed posuere consectetur est at lobortis. Morbi leo risus, porta ac consectetur.</p>-->
                                <address>
                                    <strong>Rong Tuli Events</strong><br>
                                    2nd floor, Bagdad Tanzia Tower,<br>
                                    Chandona Chowrasta, Tangail road,<br>
                                    Gazipur, Dhaka, Bangladesh <br>
                                    <abbr title="Phone">P:</abbr> +88 019 21351647 <br>
                                    <abbr title="Phone">P:</abbr> +88 016 76662596  <br>
                                    <abbr title="Phone">P:</abbr> +88 016 11365640  <br>
                                    <abbr title="Email">E:</abbr> <a href="mailto:contact@rongtulievents.com">contact@rongtulievents.com</a>
                                </address>
                            </div>
                            <!-- /.widget -->

                        </aside>
                        <!--/column -->

                    </div>
                    <!--/.row -->

                </div>
                <!--/.container -->
            </div>
            <!--/.light-wrapper -->
@endsection