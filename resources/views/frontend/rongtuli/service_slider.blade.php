
<div class="light-wrapper">
    <div class="container inner">
        <div class="carousel-wrapper">
            <div class="carousel clients">
            @foreach($services as $service)
                <a href="{{url('service/'.$service->id)}}"> <div class="item"> <img src="{{asset('public/uploads/service/'.$service->service_image)}}" alt="" style="width: 150px; height: 118px;" /> <h5 class="service_center">{{$service->service_title}}</h5> <!-- <small class="service_center">{{$service->service_description}}</small> --> </div> </a>
            @endforeach
               
            </div>
            <!--/.carousel -->
        </div>
        <!--/.carousel-wrapper -->
    </div>
    <!--/.container -->
</div>
<!-- /.light-wrapper