<div class="agile-footer">
    <div class="container">
        <div class="aglie-info-logo">
            <div class="banner-mid-wthree"> <a href="{{ url('/') }}"> <img class="footer_logo" src="{{ asset('public/images/logo_made.png') }}" alt="wp"/></a> </div>
        </div>
        <ul class="aglieits-nav">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('about') }}">About Us</a></li>
            <li><a href="{{ url('gallery') }}">Gallery</a></li>
            <li><a href="{{ url('client_contact') }}">Contact</a></li>
        </ul>
        <div class="w3_agileits_social_media">
            <ul>

                <li><a href="https://www.facebook.com/Orchid-Garden-Event-Management-1109704409064100/" class="wthree_facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="wthree_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#" class="wthree_dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                <li><a href="#" class="wthree_behance"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
            </ul>
        </div>

        <div class="copy-right">
            <p>
                <strong>Created with <i class="fa fa-heart" aria-hidden="true"></i> By <a href="http://shobarjonnoweb.com/">Shobarjonnoweb.com</a></strong>
            </p>
        </div>
    </div>
</div>