@php
     $offers = DB::table('offers')->get();
@endphp

<div class="property-grids">
    <div class="agile-homes-w3l grid">
        <a href="{{ asset('birthday_cat') }}">
            <div class="col-md-6 home-agile-left">
                <figure class="effect-moses property_height">
                    <img src="{{asset('public/images/bithday.png')}}" alt="" />
                    <figcaption>
                        <h4>Birthday Planning</h4>
                        <p>The perfect kids birthday party ideas</p>
                    </figcaption>
                </figure>
            </div>
        </a>

        <div id="video_show">
            <div class="col-md-6 home-agile-left">
                <figure class="effect-moses load_video">
                    <video poster="{{ asset('public/') }}images/page_load.gif" id="bgvid" playsinline autoplay muted loop>
                        
                        <source src="{{ asset('public/uploads/video/'.$video->video_name) }}" type="video/mp4">
                    </video>
                    <div id="polina">
                        <button class="btn btn-default">Pause</button>
                    </div>
                </figure>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-6 home-agile-text special_offer">
            <h4>Special Offers</h4>
            <ul class="list-group">
                @foreach ($offers as $offer)
                   <li class="list-group-item">
                       @php
                           print_r($offer->offer_description);
                       @endphp
                       <br>
                        
                       @php
                           print_r(substr($offer->offer_start, 0,10));
                       @endphp
                       TO
                         @php
                           print_r(substr($offer->offer_end, 0,10));
                       @endphp
                   </li>
                @endforeach
            </ul>
            {{-- <div class="date">
                <h3>TILL</h3>
                <h5><i class="fa fa-calendar" aria-hidden="true"></i> March 20 2017</h5>
            </div> --}}
            <div class="icon_wthree"><i class="fa fa-gift" aria-hidden="true"></i></div>

        </div>
        <a href="{{ url('music_cat') }}">
            <div class="col-md-6 home-agile-left">
                <figure class="effect-moses">
                    <img src="{{asset('public/images/music_n_sound.jpg')}}" alt="" />
                    <figcaption>
                        <h4>Music &amp; Sound System</h4>
                        <p>Heart of any party!</p>
                    </figcaption>
                </figure>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>
</div>

<script type="application/javascript">
    $(window).load(function(){
        var vid_height = $('.property_height').height();
        $('.load_video').css('height',vid_height+'px').css('overflow', 'hidden');
    });

    var vid = document.getElementById("bgvid");
    var pauseButton = document.querySelector("#polina button");

    if (window.matchMedia('(prefers-reduced-motion)').matches) {
        vid.removeAttribute("autoplay");
        vid.pause();
        pauseButton.innerHTML = "Paused";
    }

    function vidFade() {
        vid.classList.add("stopfade");
    }

    vid.addEventListener('ended', function()
    {
    // only functional if "loop" is removed
        vid.pause();
    // to capture IE10
        vidFade();
    });

    pauseButton.addEventListener("click", function() {
        vid.classList.toggle("stopfade");
        if (vid.paused) {
            vid.play();
            pauseButton.innerHTML = "Pause";
        } else {
            vid.pause();
            pauseButton.innerHTML = "Paused";
        }
    })
</script>