<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Sadi Mubarak</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900,400italic,500italic,700italic,900italic%7CRoboto+Slab:400,300,700%7CPlayfair+Display:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700%7COpen+Sans:400,300%7CLibre+Baskerville:400,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/simple-line-icons.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/font-awesome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/owl.carousel.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/global.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/responsive.css') }}">
	<link rel="stylesheet" type="text/less" href="{{ asset('public/assets/css/skin.less') }}">
	<link rel="stylesheet" type="text/less" href="{{ asset('public/assets/css/lsb.css') }}">

</head>

<body>
<!-- header -->
    <!-- header -->
    @include('frontend.header')
        <!--banner Section start-->

    <!--banner Section End-->

    <!--Content Area Start-->
    <div id="content">
        <!-- blog section start here -->
        <div class="w3l_inner_section">
            <div class="container">
                <div id="inner_loading">
                    <div id="inner_loading-center">
                        <div id="inner_loading-center-absolute">
                            <div class="inner_object" id="object_four"></div>
                            <div class="inner_object" id="object_three"></div>
                            <div class="inner_object" id="object_two"></div>
                            <div class="inner_object" id="object_one"></div>
                        </div>
                    </div>
                </div>
                @for ($i = 0; $i < count($galleries); $i=$i+3)
                <div class="row gallery" style="display: none">
                    <div class="inner_w3l_agile_grids">
                    @if (!empty($galleries[$i]))
                        <div class="col-md-4 w3_tabs_grid">
                            <div class="grid">
                                <a href="{{asset('public/uploads/gallery/'.$galleries[$i]->gallery_image)}}" class="lsb-preview" data-lsb-group="header">
                                    <figure class="effect-winston">
                                        <img style="height: 250px;" src="{{asset('public/uploads/gallery/'.$galleries[$i]->gallery_image)}}" class="img-responsive" alt=" " />
                                        <figcaption>
                                            <p>
                                                <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                                                <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                            </p>
                                        </figcaption>           
                                    </figure>
                                </a>
                            </div>
                        </div>
                    @endif
                    @if (!empty($galleries[$i+1]))
                        <div class="col-md-4 w3_tabs_grid">
                           <div class="grid">
                                <a href="{{asset('public/uploads/gallery/'.$galleries[$i+1]->gallery_image)}}" class="lsb-preview" data-lsb-group="header">
                                    <figure class="effect-winston">
                                        <img style="height: 250px;" src="{{asset('public/uploads/gallery/'.$galleries[$i+1]->gallery_image)}}" class="img-responsive" alt=" " />
                                        <figcaption>
                                            <p>
                                                <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                                                <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                            </p>
                                        </figcaption>           
                                    </figure>
                                </a>
                            </div>
                        </div>
                    @endif
                    @if (!empty($galleries[$i+2]))
                        <div class="col-md-4 w3_tabs_grid">
                           <div class="grid">
                                <a href="{{asset('public/uploads/gallery/'.$galleries[$i+2]->gallery_image)}}" class="lsb-preview" data-lsb-group="header">
                                    <figure class="effect-winston">
                                        <img style="height: 250px;" src="{{asset('public/uploads/gallery/'.$galleries[$i+2]->gallery_image)}}" class="img-responsive" alt=" " />
                                        <figcaption>
                                            <p>
                                                <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                                                <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                            </p>
                                        </figcaption>           
                                    </figure>
                                </a>
                            </div>
                        </div>
                    @endif
                    </div>
                    <div class="clearfix"> </div>
                </div>
                @endfor

                    </div>
                    <div class="clearfix"> </div>
                </div

            </div>
        </div>
        <!-- blog section end here -->
    </div>
    @include('frontend.footer')

	<!--Footer Section End-->
</div>

<!--Page Wrapper End-->
<script type="text/javascript" src="{{ asset('public/assets/js/jquery-2.1.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/less.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/parallax.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/jquery.appear.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/jquery.countTo.js') }}"></script>
<!-- revolution slider Js -->
<script type="text/javascript" src="{{ asset('public/assets/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/jquery.revolution.js') }}"></script>
<!--  revolution slider Js -->
<script type="text/javascript" src="{{ asset('public/assets/js/site.js') }}"></script>
<!-- Switcher Js -->
<script src="{{ asset('public/assets/js/theme-option/style-switcher/assets/js/style.switcher.js') }}"></script>
<script src="{{ asset('public/assets/js/theme-option/style-switcher/assets/js/jquery.cookie.js') }}"></script>
<!-- Switcher Js -->
<!--<script src="http://maps.googleapis.com/maps/api/js"></script>-->

<script src="{{ asset('public/assets/js/lsb.min.js') }}"></script>
<script>
    $(window).load(function() {
        $('.gallery').show();
        $('#inner_loading').hide();
        $.fn.lightspeedBox();
    });
</script>

</body>

</html>