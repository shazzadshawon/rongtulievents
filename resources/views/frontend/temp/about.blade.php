
@extends('layouts.frontend')

@section('content')





			<!--banner Section start-->
			<div class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('public/assets/images/blog-banner.jpg') }}">
				<section class="banner">

					<div class="banner-content-wrapper">
						<h1>About Us</h1>
					</div>
				</section>
			</div>
			<!--banner Section End-->

			<!--Content Area Start-->
			<div id="content">
				<!-- about us section start here -->

                <section class="activites">
                    <div class="container">
                        <div class="row">
                            <div class="header-center-content">
                                <h2>Keep in Touch</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="left-inner-header">
                                    <h4>Address</h4>
                                    <div class="span-wrapper">
                                        <a href="#"><i class="fa fa-map-marker"></i></a>
                                        <span class="village-class">414/A north kazipara, kafrul, Mirpur-10, Dhaka 1216</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="left-inner-header">
                                    <h4>Phone Number</h4>
                                    <a class="park-class" href="#">01755954243</a>
                                    <a class="park-class" href="#">01819476188</a>
                                    <a class="park-class" href="#">01620333725</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="left-inner-header">
                                    <h4>Service Hour</h4>
                                    <div class="span-wrapper">
                                        <a href="#"><i class="fa fa-clock-o"></i></a>
                                        <span class="village-class">24 hours</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

				<!-- about us section end here -->

				<!-- map section start here -->
				<div class="map-wrap">
					<div id="map"></div>
				</div>
				<!-- map section end here -->

                <!--what we do section start here -->
                <section class="what-we-do" id="what-we-do">
                    <div class="container">
                        <div class="row">
                            <div class="info-wrap">
                                <h2>WHAT WE DO</h2>
                                <p>
                                    We think every wedding and every love story is special. We love weddings, big or small, indoors or outdoors, whatever your dream is.
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="event-img">
                                    <span><img src="{{ asset('public/assets/images/gallery/wedding/gellary3.jpg') }}" alt=""></span>
                                </div>
                                <div class="event">
                                    <h3><a href="#">Wedding Consulting</a></h3>
                                    <p>
                                        The entire event is managed with meticulous care by our team leaving you free to spend time with your near and dear ones. Our highly professional and experienced team ensures 'high value for money' and gives the entire event a modern state of the art feel. We combine professionalism with personalized attention.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="event-img">
                                    <span><img src="{{ asset('public/assets/images/gallery/wedding/gellary10.jpg') }}" alt=""></span>
                                </div>
                                <div class="event padd-zero" >

                                    <h3><a href="#">Wedding Services</a></h3>
                                    <p>
                                        We provide the latest design with technology, which help create the “wow” factor for your event.
                                        It is amazing how the right lighting and sound systems can transform your event from a mundane, one among a thousand weddings to an ethereal, magical, fairy tale land where the beautiful princess ties the knot with her prince charming.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="event-img">
                                    <span><img src="{{ asset('public/assets/images/gallery/wedding/gellary4.jpg') }}" alt=""></span>
                                </div>
                                <div class="event">
                                    <h3><a href="#">Event Planning</a></h3>
                                    <p>
                                        * WEDDING PLANNING & MANAGEMENT
                                        * VENUE SELECTION
                                        * WEDDING CARDS
                                        * ACCOMMODATION
                                        * DESIGN & DÉCOR
                                        * SOUND & LIGHT
                                        * ENTERTAINMENT
                                        * FOOD & BEVERAGE
                                        * PHOTO / VIDEOGRAPHY
                                        * THEME WEDDINGS
                                        * RENTALS & TRAVELS

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--what we do section end here -->

				<!-- about us section start here -->
				<section class="event-content-section">
					<div class="container">
						<div class="row">
							<div class="header-center">
								<h2>Wedding Planning</h2>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-6">

								<div class="left-part">
									<div class="left-part-header">
										<h3><a href="#">Now we are with you!!!</a></h3>
									</div>
									<p>
										Your Wedding day symbolises the beginning of your journey through life together. As this special day draws closer, one important aspect of your preparation can be made easier for all of your wedding planning At BD Wedding Event Team, we provide abundant resources for planning your wedding. You can browse our wedding budget page and call us for more information. As many of today’s brides and grooms are busy pursuing full-time careers or higher education, time is critical. Use your time efficiently by calling us during your lunch break or early in the morning.
									</p>
									<p>
										With very little effort you have access to a wide source of wedding information for your wedding planning.  So why don’t you stay organized with a good helpful system for all your wedding planning.  From here, to swatches and photos to help you create ideas for your wedding day.  You can find information in many places, but here our web site for Bangladeshi people is a wedding guide on how to resource your wedding day.
									</p>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="right-part">
									<figure>
										<img src="{{ asset('public/assets/images/event-img-1.jpg') }}" alt="#">
									</figure>

								</div>
							</div>
						</div>
						<div class="row margin-top">
							<div class="col-xs-12 col-sm-6">
								<div class="right-part">
									<figure>
										<img src="{{ asset('public/assets/images/event-img-2.jpg') }}" alt="#">
									</figure>

								</div>
							</div>
							<div class="col-xs-12 col-sm-6">

								<div class="left-part">
									<p>
										Most of the people know what it takes to plan a wedding. However, with changing times the various demands of a wedding requirement has gone up within only 1 day notice. So, in order to make the event a memorable experience, your wedding planning shall be in sync with current demands of the time. Taking care of the same shall ensure that everyone invited over the event can have their share of enjoyment and none is made to feel like a left out
									</p>
									<p>
										We can make sure that your guests have the best possible refreshments, that please their palates, at the best possible cost to you. With our experience in planning and organizing weddings, we have a long list of available vendors and caterers who can give you exactly what you need. With top quality ingredients, the best service, and extensive refreshment menu options!
									</p>
								</div>
							</div>
						</div>
					</div>

                    <div class="container">
                        <div class="row">
                            <div class="header-center">
                                <h2>DESIGN &amp; DÉCOR</h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">Lighting that transforms your space!</a></h3>
                                    </div>
                                    <p>
                                        Good and well placed lighting, we have found, can hide all the minor faults that can often make a wedding venue unattractive. It subtly highlights the more pleasant elements that are worth showing off. We always emphasize to our clients the fact of how important the proper lighting is for their marriage location.
                                    </p>
                                    <p>
                                        It is amazing how the right lighting and sound systems can transform your event from a mundane, one among a thousand weddings to an ethereal, magical, fairy tale land where the beautiful princess ties the knot with her prince charming.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_01.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                        </div>
                        <div class="row margin-top">
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event-2.jpg') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">The makings of dreamland!</a></h3>
                                    </div>
                                    <p>
                                        It is of the utmost importance that you discuss the details with your wedding planner or get an expert professional who has experience with lighting design. There are numerous technical aspects involved which need to be handled correctly to make your event a success!
                                    </p>
                                    <p>
                                        Creating the most perfect atmosphere at your wedding, with the most beautiful ambient lighting is something we understand very well. Call us, or write to us, to see what we can do with lights. Allow us to influence the mood, evoke romance, and bring warmth to your marriage. See how we enhance the beauty of the flower arrangements, bringing them to life, and check out the dreamland we create on the stage and the dance floor.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="header-center">
                                <h2>ENTERTAINMENT</h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">Establish the Mood</a></h3>
                                    </div>
                                    <p>
                                        Before the ceremony, and during the early part of the wedding day evening, a mellow, more romantic mood needs to prevail. The appropriate choice for mood music at this time would be softer, romantic ballads and older songs, wither in vocal performance or instrumental. For Bangladeshi weddings, the Shehnai has always been the traditional and time honored choice. You could also choose some kind of music with religious significance, if that is more in keeping with your tastes. Either way, the music for this part of the evening would symbolize the importance and significance of this particular day in your life. This could be either a vocalist rendering love ballads or more ethnic music selections. The one thing this music should not be is dance oriented. Volumes should be low, making it the background sound, and the music can be coordinated to the theme you choose for the wedding.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_02.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                        </div>
                        <div class="row margin-top">
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_03.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">Get down to the floor!</a></h3>
                                    </div>
                                    <p>
                                        The later part of the evening is the time for faster paced live entertainment and music to pump up the energy and get your guests dancing. Whether you are planning a live band, or a DJ evening, the audience is sure to enjoy it. The dancing will come once everyone has had a chance to mingle, and early diners have had the chance to get their fill. Let your guests know that it is time to let their hair down with the help of some peppy and upbeat music and performance. Choices can vary, from a DJ performing amid flashing strobes and party lights to a live MC and band. You might also decide to bring in a touch of humor by getting a stand-up comic to do his bit, or go with a magician to amaze your guests and give them something to discuss and talk about.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="header-center">
                                <h2>PHOTO &amp; VIDEOGRAPHY</h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">Immortalizing your magical moments!</a></h3>
                                    </div>
                                    <p>
                                        The blissful day of your wedding is a utopian time when two lives unite, and two hearts beat as one. And wedding photo & videography is the memorializing of two people reveling in love and making a promise to share a life – for a lifetime! Our wedding photography services will preserve each magic moment for all time!
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_04.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                        </div>
                        <div class="row margin-top">
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_05.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">Trust professionals</a></h3>
                                    </div>
                                    <p>
                                        It is of the utmost importance to hire a photo & videographer who is the very best! Our wedding photo & videography services guarantee the best professional who will immortalize your day of magic. We will take the absolute highest care to make sure that the photographs commemorating the day you begin your life together are as fabulous as they should be! We will take advantage of all the photo & video opportunities, capturing all your smartly dressed guests, the genuinely happy faces and the magic of the day.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="header-center">
                                <h2>WEDDING IDEAS</h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">WEDDING GIFT IDEAS</a></h3>
                                    </div>
                                    <p>
                                        After all, it is hardly appropriate, or cool, to get the same old thing for everyone. More and more, as the Bangladeshi wedding changes its avatar from the traditional gaye holud night, décor and events, and moves towards a more open and modern approach, gift giving becomes ever more difficult.
                                        Traditionally, in an old fashioned Bangladeshi tying of the knot, there are unspoken prescribed rules about who gives what to whom. Close friends and family give jewelry to the bride, preferably gold, and other guests usually give either cash or household objects they consider necessary to a newly wed pair. While this made wedding gift giving somewhat easier, from the point of view of the couple it often meant ending up with half a dozen lamps, three toasters, five dinner sets, and so on.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_06.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                        </div>
                        <div class="row margin-top">
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_07.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">WEDDING DéCOR IDEAS</a></h3>
                                    </div>
                                    <p>
                                        Think about how much of your personalities you want reflected in the way the venues are dolled up for your various events. Keep in mind factors like season, costs, colour schemes, and so on.
                                        Your wedding reception is bound to be a really important part of your wedding process, if not the most important of the festivities. This is the event that is going to have the largest guest list, with more intimate friends and relatives being at the pre wedding gaye holud night, and it will be the one that most of them remember.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="header-center">
                                <h2>MAKE UP</h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">BRIDAL MAKE-UP</a></h3>
                                    </div>
                                    <p>
                                        It is a day you have dreamed of for years. Every bride has big plans for her wedding day, not the least of which is to look spectacular at the event and in the photographs. To be able to put in your best appearance on the special day, there is a range of techniques that must be used for days before. Various bridal make-up package in our country is the perfect tool to get you looking perfect for this most important of days.
                                    </p>
                                    <p>
                                        You have that gorgeous and stunning wedding dress and the fabulous jewellery has been made and bought. However, these, while wonderfully rich, are not enough to make you look like the goddess that you should be at the event. The correct make up as well as the right hair do are equally important. While the right makeup and hair can add volumes to the beauty of the ensemble, wrong choices can ruin the effect. It is very important that you trust only the experts, like us, with extensive
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_08.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                        </div>
                        <div class="row margin-top">
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_09.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">GROOMING THE GROOM</a></h3>
                                    </div>
                                    <p>
                                        There is a lot of attention that goes into planning the look of the bride. In any wedding, the bride is always the focus of all the attention, and has a lot of attention paid to her health and beauty. Most guides and wedding tips leave the grooms to more or less fend for themselves. However, we realize that the groom needs to look and feel good too! It is as much his special day as it is the bride’s, and a glowing and stunning bride is not going to look her best if the groom is looking under par. The prince of the event deserves a lot of quality attention too!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="header-center">
                                <h2>VENDOR MANAGEMENT</h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">Sensational selections</a></h3>
                                    </div>
                                    <p>
                                        A major part of the process of planning your wedding process is going to be the various meetings and screenings. You will have to meet different vendors, not just for the various jobs, but interview different vendors to choose one for each.
                                    </p>
                                    <p>
                                        All the different options for each item, such as catering, entertainment, venue, etc must be checked out and the best option selected. After all, successfully presenting your dream plan for your wedding will depend on the right choice of vendors who can execute your vision on the big day, as well as the other events!
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_10.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                        </div>
                        <div class="row margin-top">
                            <div class="col-xs-12 col-sm-6">
                                <div class="right-part">
                                    <figure>
                                        <img src="{{ asset('public/assets/images/event_11.png') }}" alt="#">
                                    </figure>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">

                                <div class="left-part">
                                    <div class="left-part-header">
                                        <h3><a href="#">We can help!</a></h3>
                                    </div>
                                    <p>
                                        As professional wedding planners, we can find you the top of the line vendors who are best capable of doing everything that you need, when you need it. Our list of wedding vendors is a who’s who of well established, experienced, qualified and professional vendors who are perfect for each step of the wedding process.
                                    </p>
                                    <p>
                                        As for us, we have extensive experience of dealing with and managing the various vendors that together make up the perfect wedding, and we deal with numerous quality wedding vendors practically everyday.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
				</section>
				<!-- about us section end here -->

			</div>
			<!--Content Area End-->


@endsection