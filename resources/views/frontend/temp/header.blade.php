@php
     $services = DB::table('services')->get();
@endphp

<header id="header" class="header">
    <div class="container">
        <div class="row ">
            <div class="col-xs-12">
                <div class="header-cont clearfix">
                    <a href="{{ asset('/') }}" style="height: 50px" class="logo"> <img src="{{ asset('public/assets/images/logo_t.png') }}" alt=""></a>
                    <nav>
                        <button class="home-menu" type="button">
                            <span class="icon-bar"> </span>
                            <span class="icon-bar"> </span>
                            <span class="icon-bar"> </span>
                        </button>
                        <ul class="navigation clearfix">
                            <li class="active">
                                <a href="{{ url('/') }}">HOME</a>
                            </li>
                            <li>
                                <a href="{{ url('about') }}" data-id="">ABOUT US</a>
                            </li>
                            <li>
                                <a href="{{ url('/#our-story') }}" data-id="">OUR STORY</a>
                            </li>
                            <li>
                                <a href="{{ url('/#services') }}" data-id="">SERVICES</a>
                                <ul class="drop-down">
                                    @foreach ($services as $service)
                                        <li>
                                            <a href="{{ url('service#'.$service->id) }}">{{ $service->service_title }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <a href="{{ url('/#gallary') }}" data-id="">GALLERY</a>
                            </li>
                            <li>
                                <a href="{{ url('package') }}">PACKAGES</a>
                            </li>
                            <li>
                                <a href="{{ url('/#contact') }}">CONTACT US</a>
                            </li>
<!--                            <li>-->
<!--                                <a href="#">pages</a>-->
<!--                                <ul class="drop-down">-->
<!--                                    <li>-->
<!--                                        <a href="services.html">Event</a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <a href="blog.html">Blog</a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <a href="blog-detail-page.html">Blog-Detail</a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <a href="travel-info.html">Travel-Info</a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <a href="404-page.html">404</a>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                            </li>-->
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>