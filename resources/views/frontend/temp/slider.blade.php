<div id="rev_slider_wrapper">
    <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
    <div id="slider1" class="rev_slider">

        <ul>
        @for ($i = 0; $i < count($sliders); $i++)
           

            <!-- SLIDE  -->
            @if ($sliders[$i])
                @if ($i%4 == 0)
                        <li data-transition="boxslide">
                            <img src="{{ asset('public/uploads/slider/'.$sliders[$i]->slider_image) }}" alt="">
                        </li>
                @elseif ($i%4 == 1)
                        <li data-transition="3dcurtain-horizontal">
                            <img src="{{ asset('public/uploads/slider/'.$sliders[$i]->slider_image) }}" alt="">
                        </li>

                @elseif ($i%4 == 2)
                        <li data-transition=" curtain-1">
                            <img src="{{ asset('public/uploads/slider/'.$sliders[$i]->slider_image) }}" alt="">
                        </li>
                @elseif ($i%4 == 3)
                        <li data-transition="3dcurtain-horizontal">
                            <img src="{{ asset('public/uploads/slider/'.$sliders[$i]->slider_image) }}" alt="">
                        </li>
                @endif
            @endif
           
        @endfor


        </ul>


    </div>
</div>