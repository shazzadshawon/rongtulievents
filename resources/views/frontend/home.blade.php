@extends('layouts.frontend')

@section('content')

 <!-- .revolution -->
        
        @include('frontend.rongtuli.home_slider')
        <!-- /.revolution -->

        <!-- .special offer -->
        @include('frontend.rongtuli.special_offer')
        <!-- /.special offer -->

        <!--Recent Work-->
      
        @include('frontend.rongtuli.recent_work')
        <!--/. Recent Work-->

        <!--A Little About ME-->

        <!--//A Little About ME-->

        <!--Service Slider-->
    
        @include('frontend.rongtuli.service_slider')
        <!--.Service Slider-->

        <!--Client Review-->
       
        @include('frontend.rongtuli.client_review')
        <!--./Client Review-->

@endsection