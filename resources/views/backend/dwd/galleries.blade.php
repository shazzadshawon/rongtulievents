@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                 <div class="panel panel-success">
                    

                     <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Gallery</h2>
                        </div>                                      
                        <div class="pull-right">     
                        <a href="{{ url('addgallery') }}" class="btn btn-info"><span class="fa fa-upload"></span> Add Image</a>                       
                           
                        </div>                         
                    </div>
                    
                   
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="">
                        
                       
                        <div class="gallery" id="links">
                             {{-- <div class="row"> --}}
                                 @foreach ($galleries as $gal)
                                 {{-- <div class="col-md-3"> --}}
                                    <div class="gallery-item"  title="" data-gallery>
                                            <div class="image">                              
                                                <img style="height: 200px" src="{{ asset('public/uploads/gallery/'.$gal->gallery_image) }}" alt="{{ $gal->gallery_image }}"/>                                        
                                                                                                              
                                            </div>
                                            <div class="row" align="">
                                               <a class="btn btn-success btn-xs col-md-6" tooltip="test" target="_blank" href="{{ asset('public/uploads/gallery/'.$gal->gallery_image) }}" ><span class="fa fa-eye"></span></a>
                                   
                                    <a class="btn btn-danger btn-xs  col-md-6" href="{{ url('deletegallery/'.$gal->id) }}"><span class="fa fa-trash-o"></span></a>
                                            </div>
                                                                      
                                    </div>
                                    
                                     {{-- </div> --}}
                                 @endforeach
                             {{-- </div> --}}
                           

                             
                        </div>
                             
                       {{--  <ul class="pagination pagination-sm pull-right push-down-20 push-up-20">
                            <li class="disabled"><a href="#">«</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>                                    
                            <li><a href="#">»</a></li>
                        </ul> --}}
                    </div>       
                    <!-- END CONTENT FRAME BODY -->
                </div>
                               
                </div>
            </div>
        </div>
    </div>
@endsection