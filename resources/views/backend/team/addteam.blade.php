@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add New Member</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                <form class="form-horizontal" method="POST" action="{{ url('storeteammember') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="team_title"/>
                                        </div>
                                    </div>
                                   

                                     
                                  

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Photo</label>
                                        <div class="col-md-10">
                                             <input id="name" required type="file" class="form-control" name="team_image" value=""  autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Details</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="team_description"/>
                                        </div>
                                    </div>
{{-- 
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Contact no</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="team_contact"/>
                                        </div>
                                    </div> --}}
                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-info btn-lg" name="Submit" value="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection