@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">Contact Messages</h3>
                                   
                                   {{--  <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>        --}}                         
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Mobile No</th>
                                               
                                                <th>Description</th>
                                                
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($contacts as $cat)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $cat->contact_title }}</td>
                                                <td>{{  $cat->contact_email }}</td>
                                                <td>{{  $cat->contact_phone }}</td>
                                                <td>
                                                    <?php print_r($cat->contact_description); ?>
                                                </td>
                                                <td>
                                                   
                                                    <a href="{{ url('deletecontact/'.$cat->id) }}" class="btn btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection