@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-12 ">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">Recent Work Categories</h3>
                                    <div class="pull-right">
                                         <a class="btn btn-primary"  data-toggle="modal" data-target="#modal_no_footer"><span class="fa fa-plus"></span> New Category</a>
                                    </div>
                                   {{--  <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>        --}}                         
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th style="">Title</th>
                                              
                                              <!--   <th style="width: 50%">Description</th> -->
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($work_gallery_cats as $cat)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $cat->cat_name }}</td>
                                              
                                                
                                             
                                                <td>
                                                   <!--  <a href="{{ url('editoffer/'.$cat->id) }}" class="btn btn-primary">Edit</a>
                                                    <a href="{{ url('singleoffer/'.$cat->id) }}" class="btn btn-warning">View</a>  -->
                                                     

                                                     <button type="button" class="btn btn-danger mb-control" data-box="#{{ $cat->id }}">Delete</button>
                                                   

        <div class="message-box message-box-warning animated fadeIn" id="{{ $cat->id }}">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                    <div class="mb-content">
                    <p>All Images & videos  Under this category will also be removed</p>
                        <p>Are you sure you want to delete this item ?</p>                  
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default mb-control-close">Cancel</button>
                        <a href="{{ url('deleteworkgallerycat/'.$cat->id) }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>





       <div class="modal" id="modal_no_footer" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="defModalHead">Add Category</h4>
                    </div>
                    <div class="modal-body">
                            <form class="form-horizontal" method="POST" action="{{ url('storeworkcat') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                     
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10">
                                             <input id="name" type="text" class="form-control" name="cat_name" value="" required autofocus>
                                        </div>
                                    </div>
                                  
                                                                                
                                    


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit"  value="Submit" />
                                        </div>
                                    </div>

                                    
                            </form>
                        
                    </div>
                </div>
            </div>
        </div>





@endsection