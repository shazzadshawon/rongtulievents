@extends('layouts.backend')

@section('content') 
<div class="">
<div class="row">
  <div class="col-md-12">
     <div class="panel panel-success">
    <div class="panel-heading">
    <h3> package Category</h3>
       <div class="pull-right">
      <a href="{{ url('addpackagecategory') }}" class="btn btn-info"> <span class="fa fa-plus-o"></span> New Package</a>
    </div>
    </div>
  </div>
  </div>
</div>
<div class="row">
    <div class="col-md-12">
    <hr>
    
   

    @foreach($cats as $cat)
       
        <div class="col-md-6">
         <form action="{{ url('updatepackagecategory/'.$cat->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data"">
        {{ csrf_field() }}
            <div class="panel panel-success">
          <div class="panel-heading">{{ $cat->package_title }}
          <div class="pull-right">
            <a class="btn btn-danger" href="{{url('deletepackagecategory/'.$cat->id)}}"> Delete </a>
          </div>
          </div>
          <div class="panel-body">
             <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Title</label>
                <div class="col-md-6 col-xs-12">                                            
                    <div class="input-group">
                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                        <input type="text" class="form-control" name="package_title" value="{{ $cat->package_title }}" />
                    </div>                                            
                    {{-- <span class="help-block">This is sample of text field</span> --}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Price</label>
                <div class="col-md-6 col-xs-12">                                            
                    <div class="input-group">
                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                        <input type="text" class="form-control" name="package_price" value="{{ $cat->package_price }}" />
                    </div>                                            
                    {{-- <span class="help-block">This is sample of text field</span> --}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label"></label>
                <div class="col-md-3 col-xs-12">                                            
                      <input type="submit" name="submit" value="Update" class="btn btn-info btn-block">                                        
                    {{-- <span class="help-block">This is sample of text field</span> --}}
                </div>
            </div>
                 
                    
          </div>
        </div>

                                   </form></div>   
        @endforeach

                              
    </div>
</div>
</div>
@endsection