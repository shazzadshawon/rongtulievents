@extends('layouts.backend')

@section('content') 
<div class="">
<div class="row">
    <div class="col-md-8">
    <hr>
    <h3>Update Interior Sub Category</h3>
        <form action="{{ url('updateintsubcategory/'.$subcat->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data"">
        {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Sub Category Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="sub_cat_name" value="{{ $subcat->sub_cat_name }}" />
                                            </div>                                            
                                            {{-- <span class="help-block">This is sample of text field</span> --}}
                                        </div>
                                    </div>
                                    
                                  
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Category</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control select" name="category_id">
                                                <option value="{{ $subcat->category_id }}">{{ $subcat->cat_name }}</option>
                                                @foreach ($cats as $cat)
                                                    <option value="{{ $cat->id }}">{{ $cat->cat_name }}</option>
                                                @endforeach
                                                
                                                
                                            </select>
                                            {{-- <span class="help-block">Service Type</span> --}}
                                        </div>
                                    </div>
                                    
                                   {{--  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">File</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <input type="file" class="fileinput btn-primary" name="filename" id="filename" title="Browse file"/>
                                            <span class="help-block">Input type file</span>
                                        </div>
                                    </div> --}}
                                    
                                  <div class="form-group">
                                       <label class="col-md-3 col-xs-12 control-label"></label>
                                        <div class="col-md-6 col-xs-12">                                                                                            {{-- 
                                           <button class="btn btn-primary">Clear Form</button> --}}
                                           <input type="submit" name="submit" class="btn btn-primary">
                                           {{--  pull-right --}}
                                        </div>
                                    </div>

                        
        </form>
    </div>
</div>
</div>
@endsection