@extends('layouts.backend')

@section('content') 
<div class="row clearfix">
<div class="col-md-12">
    <button class="btn btn-success pull-right" data-toggle="modal" data-target="#modal_no_footer">Add Category</button>
    <br>
</div>
</div>
    <div class="row">
        <div class="col-md-6">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Gallery Image</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storegallery') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                     
                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="gallery_image" value="" required autofocus>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Choose Category</label>
                                        <div class="col-md-10">
                                           <select name="place" required class="form-control" >
                                               @foreach ($cats as $element)
                                                   <option value="{{ $element->id }}">{{ $element->cat_name }}</option>
                                               @endforeach
                                           </select>
                                           <a href="" data-toggle="modal" data-target="#modal_no_footer">Add Category</a>
                                        </div>
                                    </div>
              
                                                                                
                                    


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" value="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
        {{-- <div class="col-md-1"></div> --}}
        <div class="col-md-6">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Gallery Video</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storegalleryvideo') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                     
                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Video</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="video_name" value="" required autofocus>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Cover Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="cover_image" value="" required autofocus>
                                        </div>
                                    </div>

                                   <div class="form-group">
                                        <label class="col-md-2 control-label">Choose Category</label>
                                        <div class="col-md-10">
                                           <select name="place" required class="form-control" >
                                               @foreach ($cats as $element)
                                                   <option value="{{ $element->id }}">{{ $element->cat_name }}</option>
                                               @endforeach
                                           </select>
                                           <a href="" data-toggle="modal" data-target="#modal_no_footer">Add Category</a>
                                        </div>
                                    </div>


                                                                                
                                    


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit"  value="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>







       <div class="modal" id="modal_no_footer" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="defModalHead">Add Category</h4>
                    </div>
                    <div class="modal-body">
                            <form class="form-horizontal" method="POST" action="{{ url('storegallerycat') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                     
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-10">
                                             <input id="name" type="text" class="form-control" name="cat_name" value="" required autofocus>
                                        </div>
                                    </div>
                                  
                                                                                
                                    


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit"  value="Submit" />
                                        </div>
                                    </div>

                                    
                            </form>
                        
                    </div>
                </div>
            </div>
        </div>



@endsection