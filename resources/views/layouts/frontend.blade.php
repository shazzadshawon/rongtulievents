<!DOCTYPE html>
<html lang="en">
  <head>
      <title>Rong Tuli</title>
      
        @include('frontend.rongtuli.head')
  </head>
  <body>
  

        
    <main class="body-wrapper">
        <!-- /.navbar -->
        
        @include('frontend/rongtuli/header')
        <!-- /.navbar -->

       @yield('content')

        <!-- /footer -->
        
        @include('frontend.rongtuli.footer')
        <!-- ./footer -->
    </main>
    <!--/.body-wrapper -->
    <script src="{{asset('public/assets/style/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/assets/style/js/plugins.js')}}"></script>
    <script src="{{asset('public/assets/style/js/classie.js')}}"></script>
    <script src="{{asset('public/assets/style/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('public/assets/style/js/scripts.js')}}"></script>
  </body>
</html>