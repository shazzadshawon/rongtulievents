<!DOCTYPE html> 
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Dashboard</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon" />
        <!-- END META SECTION -->
        
        
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('public/css/backend/theme-default.css')}}"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <style>
/* Tooltip container */
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black; /* If you want dots under the hoverable text */
}

/* Tooltip text */
.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    padding: 5px 0;
    border-radius: 6px;
 
    /* Position the tooltip text - see examples below! */
    position: absolute;
    z-index: 1;
}

/* Show the tooltip text when you mouse over the tooltip container */
.tooltip:hover .tooltiptext {
    visibility: visible;
}
</style>
        <!-- EOF CSS INCLUDE -->
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/jquery/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/jquery/jquery-ui.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/bootstrap/bootstrap.min.js')}}"></script>        
        <!-- END PLUGINS -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
    </head>
    <body>

    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1923089044631336";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
        <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
              @include('backend/sidebar');
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                  
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                   
                </ul>
          
                <div class="page-content-wrap">
                <br>
                 <div class="">
                <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                            @if (Session::has('info'))
                                <div class="alert alert-info alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        <li>{!! Session::get('info') !!}</li>
                                    </ul>
                                </div>
                            @endif
                        @if (Session::has('warning'))
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('warning') !!}</li>
                                </ul>
                            </div>
                        @endif
                            @if (Session::has('danger'))
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        <li>{!! Session::get('danger') !!}</li>
                                    </ul>
                                </div>
                            @endif
                    </div>
                </div>
            </div>
                    @yield('content')

                    <!-- START WIDGETS -->                    
                   
                    <!-- END WIDGETS -->                    
                 
                    <!-- START DASHBOARD CHART -->
                    <div class="chart-holder" id="dashboard-area-1" style="height: 200px;"></div>
                    <div class="block-full-width">
                                                                       
                    </div>                    
                    <!-- END DASHBOARD CHART -->
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>           
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            {{-- <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a> --}}
                            <a href="{{ route('logout') }}" class="btn btn-success btn-lg" 
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Yes
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src="{{asset('public/js/backend/plugins/icheck/icheck.min.js')}}"></script>        
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/morris/raphael-min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/morris/morris.min.js')}}"></script>       
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/rickshaw/d3.v3.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/rickshaw/rickshaw.min.js')}}"></script>
        <script type='text/javascript' src="{{asset('public/js/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script type='text/javascript' src="{{asset('public/js/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>                
        <script type='text/javascript' src="{{asset('public/js/backend/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>                
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/owl/owl.carousel.min.js')}}"></script>                 
        
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/moment.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/daterangepicker/daterangepicker.js')}}"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="{{asset('public/js/backend/settings.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('public/js/backend/plugins.js')}}"></script>        
        <script type="text/javascript" src="{{asset('public/js/backend/actions.js')}}"></script>
        {{-- 
        <script type="text/javascript" src="{{asset('public/js/backend/demo_dashboard.js')}}"></script> --}}
        <script type="text/javascript" src="{{asset('public/js/backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('public/js/backend/plugins/summernote/summernote.js')}}"></script>
 <script type="text/javascript" src="{{asset('public/js/backend/plugins/dropzone/dropzone.min.js')}}"></script>
 <script type="text/javascript" src="{{asset('public/js/backend/plugins/fileinput/fileinput.min.js')}}"></script>
 <script type="text/javascript" src="{{asset('public/js/backend/plugins/filetree/jqueryFileTree.js')}}"></script>

        <!--<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>-->

       {{--  <script type="text/javascript" src="js/plugins/dropzone/dropzone.min.js"></script>
        <script type="text/javascript" src="js/plugins/fileinput/fileinput.min.js"></script>        
        <script type="text/javascript" src="js/plugins/filetree/jqueryFileTree.js"></script>
 --}}
<script src="{{asset('public/templateEditor/ckeditor/ckeditor.js')}}"></script>
  <script>
            CKEDITOR.replace( 'editor1' );
            CKEDITOR.replace( 'editor2' );
            CKEDITOR.replace( 'editor3' ); 
        </script>
{{-- <script type="text/javascript" src="js/plugins/summernote/summernote.js"></script> --}}
          <script>
            $(function(){
                $("#file-simple").fileinput({
                        showUpload: false,
                        showCaption: false,
                        browseClass: "btn btn-danger",
                        fileType: "any"
                });            
                $("#filetree").fileTree({
                    root: '/',
                    script: 'assets/filetree/jqueryFileTree.php',
                    expandSpeed: 100,
                    collapseSpeed: 100,
                    multiFolder: false                    
                }, function(file) {
                    alert(file);
                }, function(dir){
                    setTimeout(function(){
                        page_content_onresize();
                    },200);                    
                });                
            });            
        </script>


        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>






